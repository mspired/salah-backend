from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.db import models

# Register your models here.
from backend.models import News, AskAnswers, AskQuestion


# NEWS
class NewsAdmin(admin.ModelAdmin):
    empty_value_display = '-empty-'
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }


admin.site.register(News, NewsAdmin)
admin.site.register(AskAnswers)
admin.site.register(AskQuestion)

