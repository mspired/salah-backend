# Generated by Django 2.0.5 on 2019-05-22 01:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0013_auto_20180902_1901'),
    ]

    operations = [
        migrations.AlterField(
            model_name='askquestion',
            name='question',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
