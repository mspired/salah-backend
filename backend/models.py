import os
import uuid
from datetime import date

from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models
from django.db.models.fields import Empty
from django.dispatch.dispatcher import receiver
from django.utils.timezone import now
from sorl.thumbnail.fields import ImageField

from salah import parameters


class SalahLeague(models.Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'league' + datePart + str(uuid.uuid4()) + '_' + extension
        return result
    class Meta:
        db_table = "salah_leagues"

    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    season = models.CharField(max_length=20, default='2017-2018', choices=(('2017-2018', '2017-2018'), ('2018-2019', '2018-2019'), ('2019-2020', '2019-2020')))
    image = ImageField(upload_to=get_upload_to, default='')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.title


class Teams(models.Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'teams' + datePart + str(uuid.uuid4()) + '_' + extension
        return result

    class Meta:
        db_table = "teams"

    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    logo = ImageField(upload_to=get_upload_to, null=True, default='teams/default.jpg')
    leagues = models.ManyToManyField(SalahLeague, through='LeaguesTeams', related_name='leagues_teams')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.title

        # @property
        # def get_points(self):
        #     return (self.wins * 3) + (self.ties)


@receiver(models.signals.post_delete, sender=Teams)
def auto_delete_teams_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.logo:
        if os.path.isfile(instance.logo.path):
            os.remove(instance.logo.path)


@receiver(models.signals.pre_save, sender=Teams)
def auto_delete_teams_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Teams.objects.get(pk=instance.pk).logo
    except Teams.DoesNotExist:
        return False

    new_file = instance.logo
    if not old_file == new_file:
        try:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
        except:
            pass


class LeaguesTeams(models.Model):
    class Meta:
        db_table = "leagues_teams"

    id = models.BigAutoField(primary_key=True)
    team = models.ForeignKey(Teams, on_delete=models.CASCADE, default=None, null=False)
    league = models.ForeignKey(SalahLeague, on_delete=models.CASCADE, default=None, null=False)
    points = models.PositiveIntegerField(default=0)
    game_played = models.PositiveIntegerField(default=0)
    wins = models.PositiveIntegerField(default=0)
    losses = models.PositiveIntegerField(default=0)
    ties = models.PositiveIntegerField(default=0)
    rank = models.PositiveIntegerField(default=0)
    goals_for = models.PositiveIntegerField(default=0)
    goals_against = models.PositiveIntegerField(default=0)
    status = models.CharField(max_length=20, choices=parameters.TEAM_STATUS_LIST,
                              default=parameters.TEAM_STATUS_HOLD)


class Match(models.Model):
    class Meta:
        db_table = "matches"

    id = models.BigAutoField(primary_key=True)
    league = models.ForeignKey(SalahLeague, on_delete=models.CASCADE, default=None, null=True, blank=True)
    venue = models.CharField(max_length=200, default=None, blank=True, null=True)
    start_date = models.DateTimeField(null=True, blank=True)
    week = models.PositiveSmallIntegerField(null=True, blank=True)
    title = models.CharField(max_length=300, blank=False, null=False)
    status = models.CharField(max_length=20, choices=parameters.MATCH_STATUS_LIST,
                              default=parameters.MATCH_STATUS_PRE)
    teams = models.ManyToManyField(Teams, through='MatchTeams', related_name='matches_teams')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    # def __str__(self):  # __unicode__ on Python 2
    #     return self.title


class MatchTeams(models.Model):
    class Meta:
        db_table = "matches_teams"

    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)
    team = models.ForeignKey(Teams, on_delete=models.CASCADE, null=False)
    # Statistics
    result = models.CharField(max_length=20, choices=parameters.RESULT_STATUS_LIST,
                              default=parameters.RESULT_STATUS_DRAW)
    score = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    assists = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    free_kicks = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    penalties = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    total_shots = models.PositiveSmallIntegerField(default=0, null=False, blank=True)

    shots_on_target = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    shots_off_target = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    corners = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    saves = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    offsides = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    fouls = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    yellow_cards = models.PositiveSmallIntegerField(default=0, null=False, blank=True)
    red_cards = models.PositiveSmallIntegerField(default=0, null=False, blank=True)

    # Predictions totals
    pred_num_of_users = models.PositiveIntegerField(default=0, null=False, blank=True)
    pred_wins = models.PositiveIntegerField(default=0, null=False, blank=True)
    pred_loses = models.PositiveIntegerField(default=0, null=False, blank=True)
    pred_ties = models.PositiveIntegerField(default=0, null=False, blank=True)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)


@receiver(models.signals.post_save, sender=MatchTeams)
def update_results_field(instance, raw, created, **kwargs):
    if created:
        try:
            team_1 = MatchTeams.objects.order_by('id').filter(match_id=instance.match)[0]
            team_2 = MatchTeams.objects.order_by('id').filter(match_id=instance.match)[1]
            if team_1.score > team_2.score:
                MatchTeams.objects.filter(id=team_1.id).update(result='WIN')
                MatchTeams.objects.filter(id=team_2.id).update(result='LOSE')
            elif team_2.score > team_1.score:
                MatchTeams.objects.filter(id=team_1.id).update(result='LOSE')
                MatchTeams.objects.filter(id=team_2.id).update(result='WIN')
            else:
                MatchTeams.objects.filter(id=team_1.id).update(result='DRAW')
                MatchTeams.objects.filter(id=team_2.id).update(result='DRAW')
        except:
            pass


class MatchDetails(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    details = RichTextUploadingField()
    image = models.ImageField(upload_to='match_details', default='')


class MatchLive(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    minute = models.IntegerField(default=1)
    type = models.CharField(max_length=100, choices=parameters.MATCH_EVENTS_TYPES)
    text = models.TextField()


class MatchTimeline(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    team = models.ForeignKey(Teams, on_delete=models.CASCADE)
    minute = models.IntegerField(default=1)
    type = models.CharField(max_length=100, choices=parameters.MATCH_EVENTS_TYPES)
    player = models.CharField(max_length=100)


class MatchPitch(models.Model):
    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    goal_keeper_name = models.TextField()
    goal_keeper_number = models.TextField()
    defenders_names = models.TextField()
    defenders_numbers = models.TextField()
    mid_names = models.TextField()
    mid_numbers = models.TextField()
    attack_names = models.TextField()
    attack_numbers = models.TextField()


class Prediction(models.Model):
    class Meta:
        db_table = "predictions"

    id = models.BigAutoField(primary_key=True)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, null=False)
    match_team = models.ForeignKey(MatchTeams, on_delete=models.CASCADE, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    score = models.PositiveSmallIntegerField(default=0)
    is_winner = models.BooleanField(default=False)

    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)


class News(models.Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'news' + datePart + str(uuid.uuid4()) + '_' + extension
        return result

    class Meta:
        db_table = "news"

    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    descr = RichTextUploadingField(null=False, blank=True, default='')
    imgid = ImageField(upload_to=get_upload_to, null=True, default='news/default.jpg')
    featured = models.BooleanField(default=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.title


@receiver(models.signals.post_delete, sender=News)
def auto_delete_news_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.imgid:
        if os.path.isfile(instance.imgid.path):
            os.remove(instance.imgid.path)


@receiver(models.signals.pre_save, sender=News)
def auto_delete_news_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = News.objects.get(pk=instance.pk).imgid
    except News.DoesNotExist:
        return False

    new_file = instance.imgid
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Videos(models.Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'videos' + datePart + str(uuid.uuid4()) + '_' + extension
        return result

    class Meta:
        db_table = "videos"

    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    video_id = models.FileField(upload_to=get_upload_to, null=True, default='videos/default.jpg')
    imgid = ImageField(upload_to=get_upload_to, null=True, default='videos/default.jpg')
    video_type = models.CharField(max_length=255, default='view1', choices=parameters.VIDEO_TYPE, null=False, blank=False)
    ftp_name = models.CharField(max_length=100, default='name', null=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.title


@receiver(models.signals.post_delete, sender=Videos)
def auto_delete_videos_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.video_id:
        if os.path.isfile(instance.video_id.path):
            os.remove(instance.video_id.path)


@receiver(models.signals.pre_save, sender=Videos)
def auto_delete_videos_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Videos.objects.get(pk=instance.pk).video_id
    except Videos.DoesNotExist:
        return False

    new_file = instance.video_id
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Gallery(models.Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'gallery' + datePart + str(uuid.uuid4()) + '_' + extension
        return result

    class Meta:
        db_table = "gallery"

    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    imgid = ImageField(upload_to=get_upload_to, null=True, default='gallery/default.jpg')
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.title


@receiver(models.signals.post_delete, sender=Gallery)
def auto_delete_gallery_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.imgid:
        if os.path.isfile(instance.imgid.path):
            os.remove(instance.imgid.path)


@receiver(models.signals.pre_save, sender=Gallery)
def auto_delete_gallery_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Gallery.objects.get(pk=instance.pk).imgid
    except Gallery.DoesNotExist:
        return False

    new_file = instance.imgid
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class SalahNumbers(models.Model):
    class Meta:
        db_table = "salah_numbers"

    id = models.BigAutoField(primary_key=True)
    key = models.CharField(max_length=255, unique=False, null=False, blank=False)
    val = models.FloatField(default=0, null=False, blank=False)
    match = models.ForeignKey(Match, on_delete=models.CASCADE, default=None, null=True, blank=True)
    league = models.ForeignKey(SalahLeague, on_delete=models.CASCADE, default=None, null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.key


class AskQuestion(models.Model):
    class Meta:
        db_table = "ask_questions"

    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    # question = models.TextField(null=False, blank=False)
    question = models.FileField(null=True, blank=True, upload_to='ask')
    answered = models.BooleanField(default=False)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.question


class AskAnswers(models.Model):
    class Meta:
        db_table = "ask_answers"

    id = models.BigAutoField(primary_key=True)
    # user = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=True)
    question = models.ForeignKey(AskQuestion, on_delete=models.CASCADE, null=False, blank=False)
    answer = models.TextField()
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.answer


@receiver(models.signals.post_save, sender=AskAnswers)
def update_answered_field(instance, raw, created, **kwargs):
    if created:
        AskQuestion.objects.filter(id=instance.question.id).update(answered=True)


class Slider(models.Model):
    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'sliders' + datePart + str(uuid.uuid4()) + '_' + extension
        return result

    class Meta:
        db_table = "sliders"

    id = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=255, unique=False, null=False, blank=False)
    imgid = ImageField(upload_to=get_upload_to, null=True, default='sliders/default.jpg')
    url = models.URLField(null=True, blank=True)
    created_at = models.DateField(auto_created=True, default=now, blank=True)
    updated_at = models.DateField(auto_now=True, blank=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.title


@receiver(models.signals.post_delete, sender=Slider)
def auto_delete_slider_file_on_delete(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.imgid:
        if os.path.isfile(instance.imgid.path):
            os.remove(instance.imgid.path)


@receiver(models.signals.pre_save, sender=Slider)
def auto_delete_videos_file_on_change(sender, instance, **kwargs):
    """Deletes file from filesystem
    when corresponding `MediaFile` object is changed.
    """
    if not instance.pk:
        return False

    try:
        old_file = Slider.objects.get(pk=instance.pk).imgid
    except Slider.DoesNotExist:
        return False

    new_file = instance.imgid
    if not old_file == new_file:
        if os.path.isfile(old_file.path):
            os.remove(old_file.path)


class Career(models.Model):
    title = models.CharField(max_length=255)
    year = models.IntegerField(default=2011)
    description = models.TextField()

    def __str__(self):
        return self.title


class Awards(models.Model):

    def get_upload_to(self, filename):
        extension = os.path.splitext(filename)[1]
        datePart = date.today().strftime("/%Y/%d/%m/")
        result = 'awards' + datePart + str(uuid.uuid4()) + '_' + extension
        return result

    title = models.CharField(max_length=120)
    year = models.IntegerField(default=2011)
    description = models.TextField()
    image = ImageField(upload_to=get_upload_to)

    career = models.ForeignKey(Career, on_delete=models.CASCADE, default=1)


class Verification(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    verified = models.BooleanField(default=False)
    code = models.CharField(max_length=6, default='0000')


class Terms(models.Model):
    text = RichTextUploadingField()


@receiver(models.signals.post_save, sender=User)
def set_verified_state(sender, instance, created, **kwargs):
    if created:
        Verification.objects.create(user=instance)


@receiver(models.signals.post_save, sender=User)
def save_verification_state(sender, instance, **kwargs):
    instance.verification.save()

