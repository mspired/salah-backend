from django.urls import path

from backend import views as backend_views

__author__ = 'ashraf'

from django.conf.urls import url, include


# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'profile', views.UserProfileViewSet)

# profiles_list = UserProfileViewSet.as_view({
#     'get': 'list'
# })

urlpatterns = [
    # Teams
    path('teams/', backend_views.TeamsList.as_view(), name='admin-teams-list'),
    path('teams/create/', backend_views.TeamCreate.as_view(), name='admin-teams-create'),
    path('teams/<int:pk>/delete/', backend_views.TeamDelete.as_view(), name='admin-teams-delete'),
    path('teams/<int:pk>/update/', backend_views.TeamUpdate.as_view(), name='admin-teams-update'),
    # TeamLeagues
    path('team-league/<int:team>/<int:league>/update/', backend_views.TeamLeagueUpdate.as_view(), name='admin-team-leagues-update'),
    # News
    path('news/', backend_views.NewsList.as_view(), name='admin-news-list'),
    path('news/create/', backend_views.NewsCreate.as_view(), name='admin-news-create'),
    path('news/<int:pk>/delete/', backend_views.NewsDelete.as_view(), name='admin-news-delete'),
    path('news/<int:pk>/update/', backend_views.NewsUpdate.as_view(), name='admin-news-update'),
    # Videos
    path('videos/', backend_views.VideosList.as_view(), name='admin-videos-list'),
    path('videos/create/', backend_views.VideoCreate.as_view(), name='admin-video-create'),
    path('videos/<int:pk>/delete/', backend_views.VideoDelete.as_view(), name='admin-video-delete'),
    path('videos/<int:pk>/update/', backend_views.VideoUpdate.as_view(), name='admin-video-update'),
    # Gallery
    path('gallery/', backend_views.GalleryList.as_view(), name='admin-gallery-list'),
    path('gallery/create/', backend_views.GalleryCreate.as_view(), name='admin-gallery-create'),
    path('gallery/<int:pk>/delete/', backend_views.GalleryDelete.as_view(), name='admin-gallery-delete'),
    path('gallery/<int:pk>/update/', backend_views.GalleryUpdate.as_view(), name='admin-gallery-update'),
    # Salah_League
    path('league/', backend_views.LeagueList.as_view(), name='admin-leagues-list'),
    path('league/create/', backend_views.LeagueCreate.as_view(), name='admin-league-create'),
    path('league/<int:pk>/delete/', backend_views.LeagueDelete.as_view(), name='admin-league-delete'),
    path('league/<int:pk>/update/', backend_views.LeagueUpdate.as_view(), name='admin-league-update'),
    # Questions (Ask Salah)
    path('question/', backend_views.QuestionsList.as_view(), name='admin-questions-list'),
    path('question/create/', backend_views.QuestionCreate.as_view(), name='admin-question-create'),
    path('question/<int:pk>/delete/', backend_views.QuestionDelete.as_view(), name='admin-question-delete'),
    path('question/<int:pk>/update/', backend_views.QuestionUpdate.as_view(), name='admin-question-update'),
    # Answers (Ask Salah)
    path('answer/', backend_views.AnswersList.as_view(), name='admin-answers-list'),
    path('answer/create/', backend_views.AnswerCreate.as_view(), name='admin-answer-create'),
    path('answer/<int:pk>/delete/', backend_views.AnswerDelete.as_view(), name='admin-answer-delete'),
    path('answer/<int:pk>/update/', backend_views.AnswerUpdate.as_view(), name='admin-answer-update'),
    # Matches
    path('match/', backend_views.MatchesList.as_view(), name='admin-matches-list'),
    path('match/create/', backend_views.CreateMatches, name='admin-match-create'),
    path('match/<int:id>/update/', backend_views.UpdateMatches, name='admin-match-update'),

    # Awards
    path('award/', backend_views.AwardsList.as_view(), name='admin-awards-list'),
    path('award/create/', backend_views.AwardCreate.as_view(), name='admin-award-create'),
    path('award/<int:pk>/delete/', backend_views.AwardDelete.as_view(), name='admin-award-delete'),
    path('award/<int:pk>/update/', backend_views.AwardUpdate.as_view(), name='admin-award-update'),

    #  Career
    path('career/', backend_views.CareerList.as_view(), name='admin-careers-list'),
    path('career/create/', backend_views.CareerCreate.as_view(), name='admin-career-create'),
    path('career/<int:pk>/delete/', backend_views.CareerDelete.as_view(), name='admin-career-delete'),
    path('career/<int:pk>/update/', backend_views.CareerUpdate.as_view(), name='admin-career-update'),

    # Match Details
    path('match-details/create/', backend_views.MatchDetailsCreate.as_view(), name='admin-match-details-create'),
    # Match Live
    path('match-live/create/', backend_views.MatchLiveCreate.as_view(), name='admin-match-live-create'),
    path('match-live/<int:pk>/delete/', backend_views.MatchLiveDelete.as_view(), name='admin-match-live-delete'),
    # Match Timeline
    path('match-timeline/create/', backend_views.MatchTimelineCreate.as_view(), name='admin-match-timeline-create'),
path('match-timeline/<int:pk>/delete/', backend_views.MatchTimelineDelete.as_view(), name='admin-match-timeline-delete'),
# Match Pitch
    path('match-pitch/create/', backend_views.MatchPatchCreate.as_view(), name='admin-match-pitch-create'),
    path('terms/update/', backend_views.update_terms, name='admin-terms-edit'),

]
