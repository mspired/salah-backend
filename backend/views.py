import json

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DeleteView, CreateView, UpdateView

from backend.models import Teams, News, Videos, Gallery, SalahLeague, AskQuestion, AskAnswers, Match, MatchTeams, \
    LeaguesTeams, Awards, Career, MatchDetails, MatchLive, MatchTimeline, MatchPitch, Terms
from salah import parameters
from salah_admin.forms.Match import MatchForm, MatchTeamForm, TermsForm, MatchDetailsForm


class TeamsList(ListView):
    model = Teams
    template_name = 'salah_admin/teams/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Teams.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-teams-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class TeamDelete(DeleteView):
    model = Teams
    success_url = reverse_lazy('admin-teams-list')

    def dispatch(self, *args, **kwargs):
        resp = super(TeamDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class TeamCreate(CreateView):
    model = Teams
    fields = ('title', 'logo', 'leagues')
    template_name = 'salah_admin/teams/create.html'
    success_url = reverse_lazy('admin-teams-list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        for league in form.cleaned_data['leagues']:
            teams_league = LeaguesTeams()
            teams_league.team = self.object
            teams_league.league = league
            teams_league.save()
        return redirect('admin-teams-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class TeamUpdate(UpdateView):
    model = Teams
    fields = ('id', 'title', 'logo', 'leagues')
    template_name = 'salah_admin/teams/edit.html'
    success_url = reverse_lazy('admin-teams-list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        for league in form.cleaned_data['leagues']:
            # delete unselected leagues
            for i in self.object.leagues.all():
                if i not in form.cleaned_data['leagues']:
                    m = LeaguesTeams.objects.get(league=i, team=self.object.id)
                    m.delete()
            teams_league = LeaguesTeams()
            teams_league.team = self.object
            # Save selected leagues
            if not league in self.object.leagues.all():
                teams_league.league = league
                teams_league.save()

        return redirect('admin-teams-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class TeamLeagueUpdate(UpdateView):
    model = LeaguesTeams
    fields = ('game_played', 'rank', 'wins', 'losses', 'ties', 'goals_for', 'goals_against', 'points', 'status')
    template_name = 'salah_admin/teams/teams_league_edit.html'
    success_url = reverse_lazy('admin-teams-list')

    def get_object(self, queryset=None):
        return LeaguesTeams.objects.get(team=self.kwargs.get('team'), league=self.kwargs.get('league'))


# News start


class NewsList(ListView):
    queryset = News.objects.all().order_by('-id')
    template_name = 'salah_admin/news/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(News.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-news-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class NewsDelete(DeleteView):
    model = News
    success_url = reverse_lazy('admin-news-list')

    def dispatch(self, *args, **kwargs):
        resp = super(NewsDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class NewsCreate(CreateView):
    model = News
    fields = ('title', 'descr', 'imgid', 'featured')
    template_name = 'salah_admin/news/create.html'
    success_url = reverse_lazy('admin-news-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class NewsUpdate(UpdateView):
    model = News
    fields = ('title', 'descr', 'imgid', 'featured')
    template_name = 'salah_admin/news/edit.html'
    success_url = reverse_lazy('admin-news-list')


# Videos start


class VideosList(ListView):
    model = Videos
    template_name = 'salah_admin/videos/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Videos.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-videos-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class VideoDelete(DeleteView):
    model = Videos
    success_url = reverse_lazy('admin-videos-list')

    def dispatch(self, *args, **kwargs):
        resp = super(VideoDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class VideoCreate(CreateView):
    model = Videos
    fields = ('title', 'video_id', 'imgid', 'video_type', 'ftp_name')
    template_name = 'salah_admin/videos/create.html'
    success_url = reverse_lazy('admin-videos-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class VideoUpdate(UpdateView):
    model = Videos
    fields = ('title', 'video_id', 'imgid', 'video_type', 'ftp_name')
    template_name = 'salah_admin/videos/edit.html'
    success_url = reverse_lazy('admin-videos-list')


# Gallery Start


class GalleryList(ListView):
    model = Gallery
    template_name = 'salah_admin/gallery/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Gallery.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-gallery-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class GalleryDelete(DeleteView):
    model = Gallery
    success_url = reverse_lazy('admin-gallery-list')

    def dispatch(self, *args, **kwargs):
        resp = super(GalleryDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class GalleryCreate(CreateView):
    model = Gallery
    fields = ('title', 'imgid',)
    template_name = 'salah_admin/gallery/create.html'
    success_url = reverse_lazy('admin-gallery-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class GalleryUpdate(UpdateView):
    model = Gallery
    fields = ('title', 'imgid',)
    template_name = 'salah_admin/gallery/edit.html'
    success_url = reverse_lazy('admin-gallery-list')


# Salah_League Start


class LeagueList(ListView):
    model = SalahLeague
    template_name = 'salah_admin/leagues/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(SalahLeague.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-leagues-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class LeagueDelete(DeleteView):
    model = SalahLeague
    success_url = reverse_lazy('admin-leagues-list')

    def dispatch(self, *args, **kwargs):
        resp = super(LeagueDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class LeagueCreate(CreateView):
    model = SalahLeague
    fields = ('title', 'season', 'image')
    template_name = 'salah_admin/leagues/create.html'
    success_url = reverse_lazy('admin-leagues-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class LeagueUpdate(UpdateView):
    model = SalahLeague
    fields = ('title', 'season', 'image')
    template_name = 'salah_admin/leagues/edit.html'
    success_url = reverse_lazy('admin-leagues-list')


# Questions (Ask_Salah) Start


class QuestionsList(ListView):
    model = AskQuestion
    template_name = 'salah_admin/questions/index.html'
    paginate_by = 10
    ordering = ('-created_at', '-id')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(AskQuestion.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-questions-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class QuestionDelete(DeleteView):
    model = AskQuestion
    success_url = reverse_lazy('admin-questions-list')

    def dispatch(self, *args, **kwargs):
        resp = super(QuestionDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class QuestionCreate(CreateView):
    model = AskQuestion
    fields = ('user', 'title', 'question', 'answered')
    template_name = 'salah_admin/questions/create.html'
    success_url = reverse_lazy('admin-questions-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class QuestionUpdate(UpdateView):
    model = AskQuestion
    fields = ('user', 'title', 'question', 'answered')
    template_name = 'salah_admin/questions/edit.html'
    success_url = reverse_lazy('admin-questions-list')


# Answers (Ask_Salah) Start


class AnswersList(ListView):
    model = AskAnswers
    template_name = 'salah_admin/answers/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(AskAnswers.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-answers-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class AnswerDelete(DeleteView):
    model = AskAnswers
    success_url = reverse_lazy('admin-answers-list')

    def dispatch(self, *args, **kwargs):
        resp = super(AnswerDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class AnswerCreate(CreateView):
    model = AskAnswers
    fields = ('answer', 'question')
    template_name = 'salah_admin/answers/create.html'
    success_url = reverse_lazy('admin-answers-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class AnswerUpdate(UpdateView):
    model = AskAnswers
    fields = ('answer', 'question')
    template_name = 'salah_admin/answers/edit.html'
    success_url = reverse_lazy('admin-answers-list')


# Matches Start

class MatchesList(ListView):
    model = Match
    template_name = 'salah_admin/matches/index.html'
    paginate_by = 10
    ordering = 'start_date'

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Match.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-matches-list')
        return super().dispatch(request)


@transaction.atomic
def CreateMatches(request):
    if request.method == 'POST':
        new_match = None
        match_data, team_1_data, team_2_data = getMatchData(request.POST)
        print(match_data)
        print(team_1_data)
        print(team_2_data)

        " handle creation of new objects "
        match_form = MatchForm(match_data)
        team_1 = MatchTeamForm(team_1_data)
        team_2 = MatchTeamForm(team_2_data)
        if match_form.is_valid():
            print('VAlid')
            new_match = match_form.save()

            if team_1.is_valid():
                print('VAlid1')
                new_team = team_1.save(commit=False)
                new_team.match = new_match
                new_team.save()
            else:
                print(team_1.errors)

            if team_2.is_valid():
                print('VAlid2')
                new_team_2 = team_2.save(commit=False)
                new_team_2.match = new_match
                new_team_2.save()
            else:
                print(team_2.errors)

        if new_match is not None:
            " create/update has succeeded "
            return redirect('admin-matches-list')
    match_form = MatchForm()
    team_match_form = MatchTeamForm()
    return render(request, 'salah_admin/matches/create.html',
                  {'form': match_form,
                   'form2': team_match_form},
                  )


def UpdateMatches(request, id=None):
    update_mode = False or (id is not None)
    if request.method == 'POST':
        new_match = None
        match_data, team_1_data, team_2_data = getMatchData(request.POST)
        print(team_1_data)
        if update_mode:
            " handle update after new data has been posted "
            match = Match.objects.get(pk=id)
            match_form = MatchForm(match_data, instance=match)
            try:
                team1 = MatchTeams.objects.get(match=match, team_id=team_1_data['team'])
                team_1 = MatchTeamForm(team_1_data, instance=team1)
            except MatchTeams.DoesNotExist:
                team_1 = MatchTeamForm(team_1_data)

            try:
                team2 = MatchTeams.objects.get(match=match, team_id=team_2_data['team'])
                team_2 = MatchTeamForm(team_2_data, instance=team2)
            except MatchTeams.DoesNotExist:
                team_2 = MatchTeamForm(team_2_data)


            if match_form.is_valid():
                print('VAlid')
                new_match = match_form.save()

                if team_1.is_valid():
                    print('VAlid1')
                    new_team = team_1.save(commit=False)
                    new_team.match = new_match
                    new_team.save()
                else:
                    print(team_1.errors)

                if team_2.is_valid():
                    print('VAlid2')
                    new_team_2 = team_2.save(commit=False)
                    new_team_2.match = new_match
                    new_team_2.save()
                else:
                    print(team_2.errors)

            if new_match is not None:
                " create/update has succeeded "
                return redirect('admin-matches-list')
    else:
        context = {}
        " request for create form. Return empty "
        match = Match.objects.get(pk=id)
        match_form = MatchForm(instance=match)
        context['form'] = match_form
        context['match_id'] = match.id
        teams = MatchTeams.objects.filter(match=match)
        print(teams)
        counter = 1
        for team in list(teams):
            team_match_form = MatchTeamForm(instance=team)
            context['form' + str(counter)] = team_match_form
            counter = counter + 1

        if counter <= 2:
            for idx in '12':
                team_match_form = MatchTeamForm()
                context['form' + idx] = team_match_form
                counter = counter + 1

        return render(request, 'salah_admin/matches/edit.html', context)

def getMatchData(post_data):
    raw = post_data.copy()
    match_data = {
        'league': raw['league'],
        'venue': raw['venue'],
        'start_date': raw['start_date'],
        'week': raw['week'],
        'title': raw['title'],
        'status': raw['status'],
    }
    team_1_data = {
        'team': raw['team'],
        'score': raw['score'],
        'assists': raw['assists'],
        'free_kicks': raw['free_kicks'],
        'penalties': raw['penalties'],
        'total_shots': raw['total_shots'],

        'corners': raw['corners'],
        'fouls': raw['fouls'],
        'offsides': raw['offsides'],
        'red_cards': raw['red_cards'],
        'yellow_cards': raw['yellow_cards'],
        'saves': raw['saves'],
        'shots_off_target': raw['shots_off_target'],
        'shots_on_target': raw['shots_on_target'],

        # 'pred_num_of_users': raw['pred_num_of_users'],
        # 'pred_wins': raw['pred_wins'],
        # 'pred_loses': raw['pred_loses'],
        # 'pred_ties': raw['pred_ties'],
    }
    team_2_data = {
        'team': raw['team2'],
        'score': raw['score2'],
        'assists': raw['assists2'],
        'free_kicks': raw['free_kicks2'],
        'penalties': raw['penalties2'],
        'total_shots': raw['total_shots2'],
        'corners': raw['corners2'],
        'fouls': raw['fouls2'],
        'offsides': raw['offsides2'],
        'red_cards': raw['red_cards2'],
        'yellow_cards': raw['yellow_cards2'],
        'saves': raw['saves2'],
        'shots_off_target': raw['shots_off_target2'],
        'shots_on_target': raw['shots_on_target2'],
        # 'pred_num_of_users': raw['pred_num_of_users2'],
        # 'pred_wins': raw['pred_wins2'],
        # 'pred_loses': raw['pred_loses2'],
        # 'pred_ties': raw['pred_ties2'],
    }
    return match_data, team_1_data, team_2_data

# Awards start

class AwardsList(ListView):
    model = Awards
    template_name = 'salah_admin/awards/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Awards.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-awards-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class AwardDelete(DeleteView):
    model = Awards
    success_url = reverse_lazy('admin-awards-list')

    def dispatch(self, *args, **kwargs):
        resp = super(AwardDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class AwardCreate(CreateView):
    model = Awards
    fields = ('title', 'description', 'image', 'year', 'career')
    template_name = 'salah_admin/awards/create.html'
    success_url = reverse_lazy('admin-awards-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class AwardUpdate(UpdateView):
    model = Awards
    fields = ('title', 'description', 'image', 'year', 'career')
    template_name = 'salah_admin/awards/edit.html'
    success_url = reverse_lazy('admin-awards-list')


# Career start
@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class CareerList(ListView):
    model = Career
    template_name = 'salah_admin/career/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Career.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-careers-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class CareerDelete(DeleteView):
    model = Career
    success_url = reverse_lazy('admin-careers-list')

    def dispatch(self, *args, **kwargs):
        resp = super(CareerDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class CareerCreate(CreateView):
    model = Career
    fields = ('title', 'description', 'year')
    template_name = 'salah_admin/career/create.html'
    success_url = reverse_lazy('admin-careers-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class CareerUpdate(UpdateView):
    model = Career
    fields = ('title', 'description', 'year')
    template_name = 'salah_admin/career/edit.html'
    success_url = reverse_lazy('admin-careers-list')


class MatchDetailsCreate(CreateView):
    model = MatchDetails
    fields = ('details', 'image')
    template_name = 'salah_admin/match_details/create.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data()
        if MatchDetails.objects.filter(match=int(self.request.GET.get('id'))).count() > 0:
            ctx['details_value'] = MatchDetails.objects.values('details', 'image').get(match=int(self.request.GET.get('id')))
        return ctx

    def get_form(self, form_class=MatchDetailsForm):
        """Return an instance of the form to be used in this view."""
        if form_class is None:
            form_class = self.get_form_class()
        match = Match.objects.get(pk=int(self.request.GET.get('id')))

        if MatchDetails.objects.filter(match=match).count() > 0:
            match_details = MatchDetails.objects.get(match=match)
            return form_class(instance=match_details)
        return form_class(**self.get_form_kwargs())

    def post(self, request, *args, **kwargs):
        match = Match.objects.get(pk=int(self.request.GET.get('id')))
        if MatchDetails.objects.filter(match=match).count() > 0:
            MatchDetails.objects.filter(match=match).update(details=request.POST['details'])
        else:
            print(request.POST)
            new_details = MatchDetails(match=match, details=request.POST['details'], image=request.FILES['image'])
            new_details.save()
        return redirect('admin-matches-list')


class MatchLiveCreate(CreateView):
    model = MatchLive
    fields = ('type', 'text', 'minute')
    template_name = 'salah_admin/match_live/create.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data()
        if MatchLive.objects.filter(match=self.request.GET.get('id')).count() > 0:
            ctx['match_live_items'] = MatchLive.objects.filter(match=self.request.GET.get('id')).order_by('-minute')
        return ctx

    def form_valid(self, form):
        self.object = form.save(commit=False)
        match = Match.objects.get(pk=int(self.request.GET.get('id')))
        self.object.match = match
        self.object.save()
        return redirect('admin-matches-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class MatchLiveDelete(DeleteView):
    model = MatchLive
    success_url = reverse_lazy('admin-matches-list')

    def dispatch(self, *args, **kwargs):
        resp = super(MatchLiveDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class MatchTimelineCreate(CreateView):
    model = MatchTimeline
    fields = ('type', 'minute', 'player')
    template_name = 'salah_admin/match_timeline/create.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data()
        if MatchTimeline.objects.filter(match=self.request.GET.get('match_id')).count() > 0:
            ctx['match_live_items'] = MatchTimeline.objects.filter(match=self.request.GET.get('match_id')).order_by('-minute')
        return ctx

    def form_valid(self, form):
        self.object = form.save(commit=False)
        match = Match.objects.get(pk=int(self.request.GET.get('match_id')))
        team = Teams.objects.get(pk=int(self.request.GET.get('team_id')))
        self.object.match = match
        self.object.team = team
        self.object.save()
        return redirect('admin-matches-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class MatchTimelineDelete(DeleteView):
    model = MatchTimeline
    success_url = reverse_lazy('admin-matches-list')

    def dispatch(self, *args, **kwargs):
        resp = super(MatchTimelineDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp

class MatchPatchCreate(CreateView):
    model = MatchPitch
    fields = ('goal_keeper_name', 'goal_keeper_number', 'defenders_names', 'defenders_numbers', 'mid_names', 'mid_numbers', 'attack_names', 'attack_numbers')
    template_name = 'salah_admin/match_pitch/create.html'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        match = Match.objects.get(pk=int(self.request.GET.get('id')))
        self.object.match = match
        self.object.save()
        return redirect('admin-matches-list')


def update_terms(request):
    terms_form = None
    current_terms = ''
    if request.method == 'GET':
        try:
            current_terms = Terms.objects.get(id=1)
            terms_form = TermsForm({'text': current_terms.text})

        except:
            pass
    elif request.method == 'POST':
        terms_form = TermsForm(request.POST)
        terms, created = Terms.objects.get_or_create(id=1, defaults={"text": 'Terms of use'})

        if terms:
            Terms.objects.filter(id=1).update(text=request.POST.get('text'))
    return render(request, 'salah_admin/terms/edit.html', {'current_terms': current_terms, 'form': terms_form})


