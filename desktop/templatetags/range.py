from django import template

from backend.models import Videos

register = template.Library()


@register.filter(name='times')
def times(number):
    return range(1, number+1)
