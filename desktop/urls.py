from django.contrib.auth.views import logout
from django.urls import path

from desktop.views.career import CareerListDesktop
from desktop.views.home import desktop_index, desktop_index2
from desktop.views.liverpool import LiverpoolVideoListExternal, LiverpoolVideoDetails
from desktop.views.login import login_user_desktop
from desktop.views.news import desktop_news, desktop_news_details
from desktop.views.numbers import DesktopMatchNumbers, DesktopNumbersList
from desktop.views.signup import signup_desktop, sms_verification_desktop
from desktop.views.terms import TermsDesktopView
from desktop.views.videos import VideoListDesktop, LiverpoolVideoListDesktop

urlpatterns = [
    path('', desktop_index, name='desktop-home'),
    path('home2', desktop_index2, name='desktop-home2'),
    path('login', login_user_desktop, name='desktop-login'),
    path('signup', signup_desktop, name='desktop-signup'),
    path('verify', sms_verification_desktop, name='desktop-verify'),
    path('logout', logout, {'next_page': 'desktop-login'}, name='desktop-logout'),
    path('news', desktop_news.as_view(), name='desktop-news'),
    path('news/<int:pk>', desktop_news_details.as_view(), name='desktop-news-single'),
    path('videos', VideoListDesktop.as_view(), name='desktop-videos'),
    path('videos-liverpool', LiverpoolVideoListDesktop.as_view(), name='desktop-videos-liverpool'),
    path('videos-liverpool/<int:pk>', LiverpoolVideoDetails.as_view(), name='desktop-liverpool'),
    path('career', CareerListDesktop.as_view(), name='desktop-career'),
    path('match-stats', DesktopMatchNumbers, name='desktop-match-stats'),
    path('numbers', DesktopNumbersList.as_view(), name='desktop-salah-numbers'),
    path('terms', TermsDesktopView.as_view(), name='desktop-terms'),

]
