from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView
from django.shortcuts import redirect

from backend.models import AskQuestion
from salah import parameters
from salah_admin.views.shared import check_validity, check_user


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class QuestionCreate(CreateView):
    model = AskQuestion
    fields = ('title', 'question')
    template_name = 'website/views/ask.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(QuestionCreate, self).form_valid(form)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(QuestionCreate, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        if check_validity(self.request):
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-signup')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-login')
        return redirect('desktop-verify')

    def get_success_url(self):
        messages.success(self.request, 'تم حفظ سؤالك بنجاح .. شكرا لك')
        return super().get_success_url()

