from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.shortcuts import redirect

from backend.models import Career, Awards
from salah import parameters
from salah_admin.views.shared import check_validity, check_user


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class CareerListDesktop(ListView):
    queryset = Career.objects.all().order_by('-year')
    template_name = 'desktop/views/career.html'
    context_object_name = 'career_list'

    def get_context_data(self, **kwargs):
        context = super(CareerListDesktop, self).get_context_data(**kwargs)
        context['awards_list'] = Awards.objects.all()
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        if check_validity(self.request):
            from django_user_agents.utils import get_user_agent
            user_agent = get_user_agent(self.request)

            if not user_agent.is_pc:
                return redirect('career-list')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-signup')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-login')
        return redirect('desktop-verify')
