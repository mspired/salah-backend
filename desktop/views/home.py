import pytz
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from backend.models import Slider, News, Videos, Match, SalahNumbers, SalahLeague, MatchTimeline, MatchTeams
import datetime

from salah import parameters
from salah_admin.views.shared import check_user, check_validity


@login_required(login_url='desktop-login', redirect_field_name='next')
def desktop_index(request):
    from django_user_agents.utils import get_user_agent
    user_agent = get_user_agent(request)

    if not user_agent.is_pc:
        return redirect('home')
    if check_user(request) == '0' and check_validity(request):
        #recent news and videos
        last_news = News.objects.order_by('-id').all()[:5]
        last_videos = Videos.objects.order_by('-id').all()[:5]

        tz = pytz.timezone('EET')

        time_now = datetime.datetime.now(tz=tz) - datetime.timedelta(hours=1)

        # latest match
        next_match = None
        try:
            if Match.objects.filter(status='In-Progress').count() > 0:
                next_match = Match.objects.filter(status='In-Progress').order_by('start_date')[0]
            else:
                next_match = Match.objects.filter(start_date__gte=time_now).order_by('start_date')[0]
        except IndexError:
            next_match = Match.objects.all().order_by('start_date').last()
        next_match_teams = []
        league = SalahLeague.objects.get(title=next_match.league)
        salah_numbers = SalahNumbers.objects.filter(league=league)
        for team in next_match.teams.all()[:2]:
            t = MatchTeams.objects.get(team=team, match=next_match)

            next_match_teams.append(team)
            next_match_teams.append(t)
        # counter

        time_match = next_match.start_date
        diff = time_match - time_now
        hours = diff.seconds // 3600
        seconds = diff.seconds - (hours * 3600)
        minutes = seconds // 60
        seconds = seconds - (minutes * 60)

        last_match_timeline = None
        if next_match.status == 'In-Progress':
            last_match_timeline = MatchTimeline.objects.filter(match=next_match, type='GOAL').order_by('-id')[:6]

        count_down = {'days': diff.days, 'hours': hours, 'minutes': minutes, 'seconds': seconds}
        context = {'news': last_news, 'videos': last_videos, 'next_match_date': next_match.start_date, 'next_match': next_match, 'next_match_teams': next_match_teams, 'next_match_league': league, 'salah_numbers': salah_numbers, 'last_match_timeline': last_match_timeline, 'count_down': count_down, 'image_host': parameters.IMAGE_URL_HOST, 'video_host': parameters.VIDEO_URL_HOST, 'time_now': time_now}
        if request.session.get('success', None):
            context['ask_success'] = True
            del request.session['success']
        return render(request, 'desktop/views/home.html', context=context)
    messages.error(request, 'برجاء الإشتراك فى الخدمة')
    return redirect('desktop-login')


@login_required(login_url='desktop-login', redirect_field_name='next')
def desktop_index2(request):
    # recent news and videos
    last_news = News.objects.order_by('-id').all()[:5]
    last_videos = Videos.objects.order_by('-id').all()[:5]
    # latest match
    now = datetime.datetime.now()
    try:
        next_match = Match.objects.filter(start_date__gt=now).order_by('start_date')[0]
    except IndexError:
        next_match = Match.objects.all().order_by('start_date').last()
    next_match_teams = []
    league = SalahLeague.objects.get(title=next_match.league)
    salah_numbers = SalahNumbers.objects.all()
    for team in next_match.teams.all()[:2]:
        next_match_teams.append(team)

    # counter
    tz = pytz.timezone('EET')

    time_now = datetime.datetime.now(tz=tz)
    time_match = next_match.start_date
    diff = time_match - time_now
    hours = diff.seconds // 3600
    seconds = diff.seconds - (hours * 3600)
    minutes = seconds // 60
    seconds = seconds - (minutes * 60)

    count_down = {'days': diff.days, 'hours': hours + 1, 'minutes': minutes, 'seconds': seconds}
    context = {'news': last_news, 'videos': last_videos, 'next_match_date': next_match.start_date,
               'next_match': next_match, 'next_match_teams': next_match_teams, 'next_match_league': league,
               'salah_numbers': salah_numbers, 'count_down': count_down, 'image_host': parameters.IMAGE_URL_HOST, 'video_host': parameters.VIDEO_URL_HOST}
    return render(request, 'desktop/views/home2.html', context=context)
