from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView

from backend.models import Videos
from salah import parameters


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class LiverpoolVideoList(ListView):
    queryset = Videos.objects.filter(video_type='view2').order_by('-id')
    template_name = 'website/views/videos_liverpool.html'
    context_object_name = 'videos_list'

    def get_context_data(self, **kwargs):
        context = super(LiverpoolVideoList, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        context['redirect_video'] = 'https://www.liverpoolfc.com/Vodafone'
        return context


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class LiverpoolVideoListExternal(ListView):
    queryset = Videos.objects.filter(video_type='view2').order_by('-id')
    template_name = 'lfctv/rtl.html'
    context_object_name = 'liverpool_videos_list'
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super(LiverpoolVideoListExternal, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        return context


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class LiverpoolVideoDetails(DetailView):
    model = Videos
    template_name = 'lfctv/desktop.html'
    context_object_name = 'video'

    def get_context_data(self, **kwargs):
        context = super(LiverpoolVideoDetails, self).get_context_data(**kwargs)
        context['video_host'] = parameters.VIDEO_URL_HOST
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['videos_list'] = Videos.objects.filter(video_type='view2').order_by('-created_at')
        return context
