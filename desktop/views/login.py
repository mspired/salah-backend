import requests
from django.contrib import messages
from django.shortcuts import render, redirect
from django.urls import reverse_lazy

from salah import settings
import time
from lxml import html, etree
from django.contrib.auth import authenticate, login, logout


def login_user_desktop(request):
    from django_user_agents.utils import get_user_agent
    user_agent = get_user_agent(request)

    if not user_agent.is_pc:
        return redirect('login')
    username = password = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user'
        response = requests.get(url, params={'phone': request.POST.get('username')})
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        if response.status_code != 200:
            return redirect('desktop-login')
        else:
            tree = etree.fromstring(response.content, parser=parser)
            response = tree.xpath("/Response/StatusCode")
            response_result = response[0].text
            if response_result == '0':
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect('home')
                else:
                    messages.error(request, 'رقم الهاتف او كلمة المرور غير صحيحه')
                    return redirect('desktop-login')
            elif response_result == '-9':
                messages.error(request, 'برجاء الاشتراك فى الخدمة')
                return redirect('desktop-signup')
            elif response_result == '1':
                return redirect('no-credit')
            messages.error(request, 'رقم الهاتف او كلمة المرور غير صحيحه')
            return redirect('desktop-login')

    return render(request, 'desktop/views/login.html')


# not working
def loginaaaaaaaaa(request):
    if request.method == 'POST':
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user'
        response = requests.get(url, params={'phone': request.POST.get('username')})
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        if response.status_code != 200:
            return redirect('login')
        else:
            tree = etree.fromstring(response.content, parser=parser)
            response = tree.xpath("/Response/StatusCode")
            response_result = response[0].text
            if response_result == '0':
                if request.session['first']:
                    return redirect('welcome')
                else:
                    return redirect('home')
            else:
                return ('signup')
    return render(request, 'website/views/login.html')

def LoginFromAPI(request):
    url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user/'
    # url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/send-sms'
    # url = 'https://jsonplaceholder.typicode.com/posts/'
    response = ''
    response = requests.get(url, params={'phone': 1002228492})
    # response = requests.post(url, data={'phone': '1095583881', 'smstext': 'ashraf emad'})
    return render(request, 'website/views/login.html')