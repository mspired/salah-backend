import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.utils.decorators import method_decorator

from backend import models
from backend.models import News, SalahNumbers, Match, MatchTeams, SalahLeague
from django.views.generic import ListView, DetailView
from salah import parameters
from salah_admin.views.shared import check_validity, check_user
from django.contrib import messages


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class ValidityMixin(object):
    def dispatch(self, *args, **kwargs):
        from django_user_agents.utils import get_user_agent
        user_agent = get_user_agent(self.request)

        if not user_agent.is_pc:
            return redirect('news-list')
        if check_validity(self.request):
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-signup')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-login')
        return redirect('desktop-verify')


class desktop_news(ValidityMixin, ListView):
    template_name = 'desktop/views/news.html'
    context_object_name = 'news_list'
    paginate_by = 5
    queryset = models.News.objects.all().order_by('-id')

    # def get_queryset(self):
    #     pageNum = self.request.GET.get('page')
    #     if pageNum is not None and int(pageNum) > 1:
    #         queryset = models.News.objects.all().order_by('-id')
    #     else:
    #         queryset = models.News.objects.all().order_by('-id')[5:10]
    #     return queryset

    def get_context_data(self, **kwargs):
        context = super(desktop_news, self).get_context_data(**kwargs)
        # Latest News
        latest_news = models.News.objects.all().order_by('-id')[:5]
        context['latest'] = latest_news

        context['image_host'] = parameters.IMAGE_URL_HOST
        league = SalahLeague.objects.get(title='الدوري الانجليزي')
        context['goals'] = SalahNumbers.objects.get(key='عدد اﻷهداف', league=league)
        last_match = Match.objects.filter(status='PLAYED').order_by('-start_date').first()
        team1 = MatchTeams.objects.values().get(match=last_match, team=last_match.teams.all()[0])
        team2 = MatchTeams.objects.values().get(match=last_match, team=last_match.teams.all()[1])
        context['team1'] = team1
        context['team2'] = team2
        context['last_match'] = last_match
        now = datetime.datetime.now()
        next_match = Match.objects.filter(start_date__gte=now).order_by('start_date')[0]
        context['next_match'] = next_match
        return context


class desktop_news_details(ValidityMixin, DetailView):
    model = News
    template_name = 'desktop/views/news-details.html'
    context_object_name = 'news_object'

    def get_context_data(self, **kwargs):
        context = super(desktop_news_details, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        last_match = Match.objects.filter(status='PLAYED').order_by('-start_date').first()
        team1 = MatchTeams.objects.values().get(match=last_match, team=last_match.teams.all()[0])
        team2 = MatchTeams.objects.values().get(match=last_match, team=last_match.teams.all()[1])
        context['team1'] = team1
        context['team2'] = team2
        context['last_match'] = last_match
        context['other_news'] = News.objects.all().order_by('-id')[0:3]
        context['featured'] = News.objects.filter(featured=True).order_by('-id')[0:5]
        return context
