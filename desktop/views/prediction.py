from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from backend.models import Match, MatchTeams
from salah import parameters
from website.forms.Prediction import PredictionForm


@login_required(login_url='login', redirect_field_name='next')
def predict(request):
    next_match = Match.objects.order_by('id').last()
    context = {}
    counter = 1
    for team in next_match.teams.all():
        context['team{}'.format(str(counter))] = team
        counter += 1
    if request.method == 'POST':
        team_1_data, team_2_data = getPredictData(request.POST)
        score_1 = PredictionForm(team_1_data)
        score_2 = PredictionForm(team_2_data)

        if score_1.is_valid():
            new_team = score_1.save(commit=False)
            new_team.match = next_match
            new_team.match_team = MatchTeams.objects.get(team=context['team1'].id, match=next_match.id)
            new_team.user = request.user
            new_team.save()
        if score_2.is_valid():
            new_team2 = score_2.save(commit=False)
            new_team2.match = next_match
            new_team2.match_team = MatchTeams.objects.get(team=context['team2'].id, match=next_match.id)
            new_team2.user = request.user
            new_team2.save()
            messages.success(request, 'تم حفظ توقعك بنجاح .. شكرا لك')

    context['match'] = next_match
    context['image_host'] = parameters.IMAGE_URL_HOST
    return render(request, 'website/views/prediction.html', context)


def getPredictData(post_data):
    raw = post_data.copy()
    team_1_data = {
        'score': raw['score1'],
    }
    team_2_data = {
        'score': raw['score2'],
    }
    return team_1_data, team_2_data
