import requests
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView

from backend.models import Verification
from website.forms.SMS import SMSForm
from lxml import html, etree
from random import randint
import re


def signup_desktop(request):
    from django_user_agents.utils import get_user_agent
    user_agent = get_user_agent(request)

    if not user_agent.is_pc:
        return redirect('signup')
    if request.method == 'POST':
        try:
            num = float(request.POST.get('username'))
        except:
            messages.error(request, 'برجاء تحويل لوحة المفاتيح الى اللغة الإنجليزية وإعادة المحاولة')
            return redirect('desktop-signup')
        form = UserCreationForm(request.POST)
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user'
        response = requests.get(url, params={'phone': request.POST.get('username')})
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        if response.status_code != 200:
            pass
        else:
            tree = etree.fromstring(response.content, parser=parser)
            response = tree.xpath("/Response/StatusCode")
            response_result = response[0].text
            if response_result == '0':
                if User.objects.filter(username=request.POST['username']).count() > 0:
                    return redirect('desktop-login')
                else:
                    if form.is_valid():
                        form.save()
                        username = form.cleaned_data.get('username')
                        raw_password = form.cleaned_data.get('password1')
                        user = authenticate(username=username, password=raw_password)
                        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                        # request.session['username'] = username
                        return redirect('desktop-verify')
            elif response_result == '-9':
                if User.objects.filter(username=request.POST.get('username')).exists():
                    return redirect('desktop-login')
                if form.is_valid():
                    form.save()
                    username = form.cleaned_data.get('username')
                    raw_password = form.cleaned_data.get('password1')
                    user = authenticate(username=username, password=raw_password)
                    login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                    subscribe_url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/subscribe'
                    subscribe = requests.post(subscribe_url, data={'Phone': request.user.username})
                    if subscribe.status_code == 200:
                        return redirect('desktop-verify')
            elif response_result == '1':
                if User.objects.filter(username=request.POST['username']).count() > 0:
                    return redirect('desktop-login')
                else:
                    if form.is_valid():
                        form.save()
                        username = form.cleaned_data.get('username')
                        raw_password = form.cleaned_data.get('password1')
                        user = authenticate(username=username, password=raw_password)
                        login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                        # request.session['username'] = username
                        return redirect('desktop-verify')
            return redirect('desktop-signup')
    else:
        form = UserCreationForm()
    return render(request, 'desktop/views/signup.html', {'form': form})


class Welcome(TemplateView):
    template_name = 'website/views/welcome_video.html'


def sms_verification_desktop(request):
    if request.method == 'POST':
        form = SMSForm(request.POST)
        if form.is_valid():
            response = Verification.objects.get(user=request.user)
            if request.POST.get('code') == response.code:
                user = User.objects.get(username=request.user.username)
                verified_state = Verification.objects.get(user=user)
                verified_state.verified = True
                verified_state.save()
                return redirect('home')
    else:
        form = SMSForm()
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/send-sms'
        rand = randint(1000, 9999)  # randint is inclusive at both ends
        message_to_send = requests.post(url, data={'phone': request.user.username, 'smstext': 'Please Use this as your Confirmation Code: {}'.format(str(rand))})
        if message_to_send.status_code != 200:
            return redirect('desktop-verify')
        else:
            try:
                user_verified = User.objects.get(username=request.user.username)
            except:
                return redirect('desktop-login')
            verified_state = Verification.objects.get(user=user_verified)
            verified_state.code = rand
            verified_state.save()
    return render(request, 'desktop/views/sms.html', {'form': form})




