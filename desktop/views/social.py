from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from salah import parameters


@login_required
def social(request):
    return render(request, 'website/views/social.html', {'image_host': parameters.IMAGE_URL_HOST})