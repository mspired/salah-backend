from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView

from backend.models import News, Videos
from salah import parameters
from salah_admin.views.shared import check_validity, check_user
from django.shortcuts import redirect
from django.contrib import messages


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class ValidityMixin(object):
    def dispatch(self, *args, **kwargs):
        from django_user_agents.utils import get_user_agent
        user_agent = get_user_agent(self.request)

        if not user_agent.is_pc:
            return redirect('videos-list')
        if check_validity(self.request):
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-signup')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('desktop-login')
        return redirect('desktop-verify')


class VideoListDesktop(ValidityMixin, ListView):
    queryset = Videos.objects.filter(video_type='view1').order_by('-created_at')
    template_name = 'desktop/views/videos.html'
    context_object_name = 'videos_list'
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super(VideoListDesktop, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        context['last_video'] = Videos.objects.filter(video_type='view2').order_by('-id').first()
        return context


class LiverpoolVideoListDesktop(ValidityMixin, ListView):
    queryset = Videos.objects.filter(video_type='view2').order_by('-created_at')
    template_name = 'desktop/views/liverpool_videos.html'
    context_object_name = 'videos_list'
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super(LiverpoolVideoListDesktop, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        context['last_video'] = Videos.objects.filter(video_type='view2').order_by('-id').first()
        return context


class VideoDetails(ValidityMixin, DetailView):
    model = Videos
    template_name = 'desktop/views/video-play.html'
    context_object_name = 'video_object'

    def get_context_data(self, **kwargs):
        context = super(VideoDetails, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        return context

