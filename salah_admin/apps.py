from django.apps import AppConfig


class SalahAdminConfig(AppConfig):
    name = 'salah_admin'
