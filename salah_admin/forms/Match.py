from backend.models import Match, MatchTeams, Terms, MatchDetails

__author__ = 'ashraf'

from django import forms


class MatchForm(forms.ModelForm):
    class Meta:
        model = Match
        fields = ['league', 'title', 'venue', 'start_date', 'week', 'status']


class MatchTeamForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(MatchTeamForm, self).__init__(*args, **kwargs)
        # there's a `fields` property now
        self.fields['team'].required = True

    class Meta:
        model = MatchTeams
        fields = ['team', 'score', 'assists', 'free_kicks', 'penalties', 'total_shots', 'pred_num_of_users', 'corners', 'fouls', 'offsides', 'red_cards', 'yellow_cards', 'saves', 'shots_off_target', 'shots_on_target',
                  'pred_wins', 'pred_loses', 'pred_ties']


class TermsForm(forms.ModelForm):
    class Meta:
        model = Terms
        fields = ['text', ]


class MatchDetailsForm(forms.ModelForm):
    class Meta:
        model = MatchDetails
        fields = ['details', 'image']