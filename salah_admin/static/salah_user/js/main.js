function addRate(element,rate) {
	Chart.types.Doughnut.extend({
		name: "DoughnutTextInside",
		showTooltip: function() {
			this.chart.ctx.save();
			Chart.types.Doughnut.prototype.showTooltip.apply(this, arguments);
			this.chart.ctx.restore();
		},
		draw: function() {
			Chart.types.Doughnut.prototype.draw.apply(this, arguments);

			var width = this.chart.width,
			height = this.chart.height;

			var fontSize = (height / 45).toFixed(2);
			this.chart.ctx.font = fontSize + "em UniSansBoldItalic";
			this.chart.ctx.textBaseline = "middle";
			this.chart.ctx.segmentStrokeWidth = 0;

			var text = rate + "%",
			textX = Math.round((width - this.chart.ctx.measureText(text).width) / 2),
			textY = height / 2;

			this.chart.ctx.fillText(text, textX, textY);
		}
	});

	var data = [{
		value: rate,
		color: "#f8e187",
		segmentStrokeWidth:0
	}, {
		value: 100 - rate,
		color: "#fff",
		segmentStrokeWidth:0
	}];
	var ctx = $(element)[0].getContext('2d');
	var DoughnutTextInsideChart = new Chart(ctx).DoughnutTextInside(data, {
		// responsive: true,
		showTollkit:false,
		showTooltips: false,
		percentageInnerCutout : 90,
		segmentStrokeColor: "rgba(0,0,0,0)",
		strokeWidth: 0,
		segmentStrokeWidth: 0,
	});
}