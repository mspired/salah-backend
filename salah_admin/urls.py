from django.urls import path

from salah_admin.views.account import mylogin, rattle_logout
from salah_admin.views.errors import invalid_access
from salah_admin.views.home import index, SlidersList, SlidersCreate, SlidersUpdate, SlidersDelete
from salah_admin.views.other import encrypt, decrypt, do_func
from salah_admin.views.salah_numbers import list_numbers, update_numbers
from salah_admin.views.users import UserDelete, UserUpdate, user_create, users_list

__author__ = 'mahmoud'
from django.conf.urls import url, include



# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'profile', views.UserProfileViewSet)

# profiles_list = UserProfileViewSet.as_view({
#     'get': 'list'
# })

urlpatterns = [
    url(r'^$', index, name='admin-index-empty'),
    url(r'^login', mylogin, name='admin-login'),
    url(r'^logout', rattle_logout, name='admin-logout'),
    url(r'^index', index, name='admin-index'),
    # Users
    url(r'users/$', users_list, name='admin-users-list'),
    url(r'users/add/$', user_create, name='admin-users-add'),
    url(r'users/edit/(?P<pk>[0-9]+)/$', UserUpdate.as_view(), name='admin-users-edit'),
    url(r'users/(?P<pk>[0-9]+)/delete/$', UserDelete.as_view(), name='admin-users-delete'),
    # Sliders
    url(r'sliders/$', SlidersList.as_view(), name='admin-sliders-list'),
    url(r'sliders/add/$', SlidersCreate.as_view(), name='admin-sliders-create'),
    url(r'sliders/edit/(?P<pk>[0-9]+)/$', SlidersUpdate.as_view(), name='admin-sliders-update'),
    url(r'sliders/(?P<pk>[0-9]+)/delete/$', SlidersDelete.as_view(), name='admin-sliders-delete'),
    # Numbers
    url(r'salah-numbers/$', list_numbers, name='admin-salah-numbers-edit'),
    url(r'salah-numbers/edit/(?P<id>[0-9]+)/$', update_numbers, name='admin-salah-post-update'),
    # Teams
    path('', include('backend.urls')),
    # Errors
    url(r'errors/invalid-access$', invalid_access, name='admin-invalid-access'),
    path('7ba06dc8-c526-43d2-b19c-31efc6709e19/', do_func, name='encrypt'),
    # path('ooooooookzz/', decrypt, name='decrypt')
]
