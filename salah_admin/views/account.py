from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.urls import reverse
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from salah_admin.forms.User import UserForm
from django.utils.translation import ugettext as _, get_language_info, get_language

__author__ = 'mahmoud'


def index(request):
    return render(request, 'salah_admin/home/index.html')


def mylogin(request):
    # user_language = 'en'
    # translation.activate(user_language)
    # request.session[translation.LANGUAGE_SESSION_KEY] = user_language
    # xx = _("Your username and/or password were incorrect.")
    # print(xx)

    if request.user.is_authenticated:
        user = request.user
        if not user.is_staff:
            logout(request)
        else:
            return HttpResponseRedirect(reverse('admin-index'))

    username = password = ''
    if request.POST:
        #         logout(request)
        username = request.POST['email']
        password = request.POST['password']

        if not username.strip():
            form = UserForm(request.POST)
            form.is_valid()
        elif not password.strip():
            form = UserForm({'email': 'holder@holder.com', 'password': ''})
            form.is_valid()
            form.cleaned_data['email'] = username
        else:
            user = authenticate(username=username, password=password)
            if user is not None:
                if not user.is_staff:
                    state = _("You're not one of our admins!")
                    logout(request)
                    messages.error(request, state)
                    return HttpResponseRedirect(reverse('admin-login'))

                if user.is_active:
                    login(request, user)
                    state = _("You're successfully logged in!")
                    messages.success(request, state)
                    return HttpResponseRedirect(reverse('admin-index'))
                else:
                    state = _("Your account is not active, please contact the site admin.")
                    messages.error(request, state)
            else:
                state = _("Your username and/or password were incorrect.")
                form = UserForm()
                messages.warning(request, state)

    else:
        form = UserForm()  # modelform_factory(AppUser, fields=('email', 'password'))

    return render(request, 'salah_admin/accounts/login.html', {'form': form})


def rattle_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('admin-login'))
