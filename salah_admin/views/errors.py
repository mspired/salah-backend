from django.shortcuts import render

__author__ = 'mahmoud'


def invalid_access(request):
    return render(request, 'salah_admin/errors/invalid_access.html')
