import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.db import connection
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.urls.base import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.views.generic.list import ListView

from backend.models import Slider
from salah_admin.views.shared import check_is_admin

__author__ = 'mahmoud'


@login_required(login_url='admin-login', redirect_field_name='next')
def index(request):
    goTo = check_is_admin(request, show_error_page=False)
    if goTo is not None:
        return redirect(goTo)

    context = {}
    totalUsers = User.objects.filter(is_staff=False).count()
    context['totalUsers'] = totalUsers
    context['todayUsers'] = 0
    context['todayReviews'] = 0
    context['todayShares'] = 0

    # Last N new users
    cursor = connection.cursor()
    # Total N Users
    sql = "select count(*) as c, to_char(date_joined, '%Y-%m-%d') as diff from auth_user group by diff order by diff desc limit 5 "
    cursor.execute(sql)
    last_n_users_count = []
    for row in cursor.fetchall():
        last_n_users_count.append({'count': row[0], 'day': row[1]})
    context['last_n_users_count'] = last_n_users_count

    context['last_n_movies_count'] = 0
    context['top_n_cats_count'] = 0

    return render(request, 'salah_admin/home/index.html', context)


class SlidersList(ListView):
    model = Slider
    template_name = 'salah_admin/sliders/index.html'
    paginate_by = 10

    def dispatch(self, request, *args, **kwargs):
        paginator = Paginator(Slider.objects.all(), 10)
        page = request.GET.get('page')
        if page is not None:
            if int(page) > paginator.num_pages:
                return redirect('admin-sliders-list')
        return super().dispatch(request)


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class SlidersDelete(DeleteView):
    model = Slider
    success_url = reverse_lazy('admin-sliders-list')

    def dispatch(self, *args, **kwargs):
        resp = super(SlidersDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            return resp


class SlidersCreate(CreateView):
    model = Slider
    fields = ('title', 'imgid', 'url')
    template_name = 'salah_admin/sliders/create.html'
    success_url = reverse_lazy('admin-sliders-list')


@method_decorator(login_required(login_url='admin-login', redirect_field_name='next'), name='dispatch')
class SlidersUpdate(UpdateView):
    model = Slider
    fields = ('title', 'imgid', 'url')
    template_name = 'salah_admin/sliders/edit.html'
    success_url = reverse_lazy('admin-sliders-list')