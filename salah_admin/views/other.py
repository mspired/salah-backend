import base64

from django.http import HttpResponse
from django.shortcuts import render
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
def rc4_crypt(data, key):
    if type(data) != str:
        data = data.decode('utf-8')
    S = list(range(256))
    j = 0
    out = []

    # KSA Phase
    for i in range(256):
        j = (j + S[i] + ord(key[i % len(key)])) % 256
        S[i], S[j] = S[j], S[i]

    # PRGA Phase
    i = j = 0
    for char in data:
        i = (i + 1) % 256
        j = (j + S[i]) % 256
        S[i], S[j] = S[j], S[i]
        # print("Char to be encrypted: ", char)
        # print("Ord Char", ord(char))
        TempChar = ord(char)
        # print("Key ", S[(S[i] + S[j]) % 256])
        TempS = S[(S[i] + S[j]) % 256]
        XOR = (TempChar ^ TempS)
        # print("Xor output ", XOR)
        out.append(chr(XOR))

    return ''.join(out)


# function that encrypts data with RC4 and decodes it in base64 as default
# for other types of data encoding use a different encode parameter
# Use None for no encoding
@python_2_unicode_compatible
def encrypt(request, encode=base64.b64encode):
    if request.method == 'POST':
        data = rc4_crypt(request.POST.get('value'), request.POST.get('key'))
        data = encode(str.encode(data))
        return HttpResponse(data)
    return render(request, 'admin/enc_dec.html')

@python_2_unicode_compatible
def decrypt(request, decode=base64.b64decode):
    if request.method == 'POST':
        data = decode(request.POST.get('value'))
        return HttpResponse(rc4_crypt(data, request.POST.get('key')))
    return render(request, 'admin/enc_dec.html')


def do_func(request):
    if request.method == 'POST':
        if request.POST['type'] == 'enc':
            return encrypt(request)
        elif request.POST['type'] == 'dec':
            return decrypt(request)
    return render(request, 'admin/enc_dec.html')