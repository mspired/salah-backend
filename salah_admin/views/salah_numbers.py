import json
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from backend.models import Slider, SalahNumbers, SalahLeague
from salah_admin.views.shared import check_is_admin

__author__ = 'mahmoud'


@login_required(login_url='admin-login', redirect_field_name='next')
def list_numbers(request):
    goTo = check_is_admin(request, show_error_page=False)
    if goTo is not None:
        return redirect(goTo)

    items = {}
    last_item_league = None
    numbers = SalahNumbers.objects.order_by('league')
    for item in numbers:
        if last_item_league is None or last_item_league != item.league:
            last_item_league = item.league

        if last_item_league in items:
            items[last_item_league].append(item)
        else:
            items[last_item_league] = [item]

    print(items)
    context = {'items': items}
    return render(request, 'salah_admin/salah_numbers/edit.html', context)


@login_required(login_url='admin-login', redirect_field_name='next')
def update_numbers(request, id):
    goTo = check_is_admin(request, show_error_page=False)
    if goTo is not None:
        return redirect(goTo)

    league = SalahLeague.objects.get(pk=id)

    data = request.POST
    for key in data:
        if is_number(data[key]) or isinstance(data[key], float):
            SalahNumbers.objects.filter(league=league, key=key).update(val=data[key])

    return redirect('admin-salah-numbers-edit')


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
