from django.contrib.auth import logout
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect
from backend.models import Verification
from random import randint
import requests
from lxml import html, etree


__author__ = 'mahmoud'


def check_is_admin1(request, show_error_page=True):
    user = request.user
    print(request.user)
    if not user.is_staff:
        if show_error_page:
            return redirect('admin-invalid-access')
        else:
            logout(request)
            return redirect('admin-login')


def check_is_admin(request, show_error_page=True):
    user = request.user
    if not user.is_staff:
        if show_error_page:
            return 'admin-invalid-access'
        else:
            logout(request)
            return 'admin-login'
    else:
        return None


def check_validity(request):
    user = request.user
    if request.META.get('HTTP_X_VF_MSISDN', None):
        if check_header(request) == 'success':
            return True
    else:
        if not user.is_anonymous:
            user_verified = Verification.objects.get(user=user)
            state = user_verified.verified
            return state
    return 'failed'


def check_user(request):
    if request.user.is_authenticated:
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user'
        response = requests.get(url, params={'phone': request.user.username})
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        tree = etree.fromstring(response.content, parser=parser)
        response = tree.xpath("/Response/StatusCode")
        response_result = response[0].text
        return response_result
    else:
        if check_header(request) == 'success':
            return '0'
        else:
            return redirect('login')
    # if response_result == '0':
    #     return 'active'
    # elif response_result == '-9':
    #     return 'not exist'
    # elif response_result == '1':
    #     return 'not active'


def check_header(request):
    import json
    import xmltodict
    import urllib.request
    from django.contrib.auth.models import User
    from django.contrib import messages
    from django.contrib.auth import login
    if request.META.get('HTTP_X_VF_MSISDN', None):
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/decrypt-and-checkuser?EncryptedPhone={}'.format(
        request.META['HTTP_X_VF_MSISDN'])

        dumpped = json.dumps(xmltodict.parse(urllib.request.urlopen(url)))
        loaded = json.loads(dumpped)
        try:
            user = User.objects.get(username='0{}'.format(loaded['Response']['MSISDN']))
        except:
            user = None
        if user:
            if loaded['Response']['StatusCode'] == '0':
                login(request, user)
                return 'success'
            elif loaded['Response']['StatusCode'] == '-9':
                messages.error(request, 'من فضلك سجل بياناتك')
                return redirect('signup')
            elif loaded['Response']['StatusCode'] == '1':
                return redirect('no-credit')
            else:
                messages.error(request, 'Please enter valid number or subscribe to service')

        else:

            if loaded['Response']['StatusCode'] == '0':
                sms_url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/send-sms'
                rand = randint(100000, 999999)  # randint is inclusive at both ends
                message_to_send = requests.post(sms_url, data={'phone': loaded['Response']['MSISDN'],
                                                               'smstext': 'Please use this password for Mohamed Salah Portal in case of WIFI login: {}'.format(
                                                                   str(rand))})
                if message_to_send.status_code != 200:
                    return redirect('login')
                else:
                    user = User(username='0{}'.format(loaded['Response']['MSISDN']), password=str(rand))
                    user.save()
                    user = User.objects.get(username='0{}'.format(loaded['Response']['MSISDN']))
                    login(request, user)
                    return 'success'

            elif loaded['Response']['StatusCode'] == '-9':
                messages.error(request, 'انت غير مشترك فى الخدمة من فضلك سجل بياناتك')
                return redirect('signup')
            elif loaded['Response']['StatusCode'] == '1':
                return redirect('no-credit')
    else:
        return redirect('login')