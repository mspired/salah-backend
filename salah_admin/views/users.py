import json
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, InvalidPage
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse

# Create your views here.
from django.urls.base import reverse_lazy
from django.views.generic.edit import UpdateView, DeleteView
from salah import parameters
from backend.models import User
from salah_admin.forms.User import RegisterForm
from salah_admin.views.shared import check_is_admin


@login_required(login_url='admin-login', redirect_field_name='next')
def users_list(request):
    goTo = check_is_admin(request, show_error_page=True)
    if goTo is not None:
        return redirect(goTo)

    items = User.objects.filter(is_staff=False).order_by('-date_joined')
    paginator = Paginator(items, 10)

    page = 1
    query = request.GET.get('page')
    if query is not None:
        page = query

    try:
        items = paginator.page(page)
    except (EmptyPage, InvalidPage):
        items = paginator.page(paginator.num_pages)

    # Pages
    nxt = int(page) + 1
    prev = int(page) - 1
    pages = []
    for x in range(0, paginator.num_pages):
        pages.append(x + 1)

    context = {'object_list': items, 'pageNum': int(page), 'num_of_pages': int(paginator.num_pages),
               'total': paginator.count, 'pages': pages, 'nxt': nxt, 'prev': prev}
    return render(request, 'salah_admin/users/index.html', context)


def user_create(request):
    if request.method == 'POST':
        data = request.POST
        uf = RegisterForm(request.POST)
        # uf.fields['username'] = data['password']
        if uf.is_valid():
            user = uf.save(commit=False)
            user.set_password(data['password'])
            user.username = data['email']
            user.is_superuser = False
            user.is_active = 1
            user.is_staff = False
            user.save()

            return HttpResponseRedirect(reverse('admin-users-list'))
    else:
        uf = RegisterForm()

    return render(request, 'salah_admin/users/create.html', {'form': uf})


class UserUpdate(UpdateView):
    model = User
    template_name = "salah_admin/users/edit.html"
    fields = ['first_name', 'last_name', 'email']

    def get_success_url(self):
        return reverse('admin-users-edit', args=(self.object.id,))

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.username = instance.email
        messages.success(self.request, 'Success')
        return super(UserUpdate, self).form_valid(form)


class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('admin-users-list')

    # allow delete only logged in user by appling decorator
    # @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        # maybe do some checks here for permissions ...

        resp = super(UserDelete, self).dispatch(*args, **kwargs)
        if self.request.is_ajax():
            response_data = {"result": "ok"}
            return HttpResponse(json.dumps(response_data),
                                content_type="application/json")
        else:
            # POST request (not ajax) will do a redirect to success_url
            return resp

