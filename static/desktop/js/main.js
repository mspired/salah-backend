/**
 * @Project        mohamed-salah
 * @Author         Maged Mohamed
 * @Job Title      MEAN Stack Developer
 * @Build          Wed, Aug 8, 2018 7:15 PM ET
 **/
var width = $(window).width();

$(document).ready(function() {
  // news carousel
  if ($('.owl-videos')) {
    $('.owl-videos').owlCarousel({
      rtl: true,
      loop: true,
      items: 4,
      margin: 37,
      center: false,
      nav: true,
      dots: false,
      autoplay: true,
      autoplayTimeout: 3000,
      autoplayHoverPause: true,
      stagePadding: 0,
      responsive:{
        0:{
          items:2,
          nav: false
        },
        768:{
          items:3,
          nav: true
        },
        992:{
          items:4,
        }
      }
    });
  }

  // responsive menu
  if (width <= 768) {
    resMenu()
  }

  // Open Default Tab
  if ($('#selectDefault')) {
    $('#selectDefault').click();
    $('#selectDefault').addClass("active");
  }
  
  // Open Default Accordion
  if ($("#openDefault") || $(".accordion")) {
    // $(".accordion").removeClass('active').find(".accordion-panel").css({"max-height": "0"})
    $("#openDefault").click();
  }

});

function resMenu() {
  let li = $(".heroArea nav ul li").not(':first');
  let searchForm = $(".heroArea nav form");
  
  li.remove();
  searchForm.remove();
  
  $(`<div class="res-menu"><i class="fas fa-bars"></i></div>`).insertAfter(".heroArea nav ul");
  $(`
    <menu class="resMenu">
      <i id="close-menu" class="fas fa-times"></i>
      <ul>

      </ul>
    </menu>
  `).insertBefore(".heroArea");
  $("menu ul").append(li);
}

$(document).on('click', ".res-menu", openListener);
$(document).on('click', "#close-menu", closeListener);

function openListener(event) {  
  $("html body").css({
    "overflow-y": "hidden"
  })
  let resMenu = $(".resMenu");
  resMenu.animate({left: "0"});
  $('html').on('click',function() {
    resMenu.animate({left: "-50vw"});
    $("html body").css({
      "overflow-y": "auto"
    })
  });
}

function closeListener(event) {
  let resMenu = $(".resMenu");
  resMenu.animate({left: "-50vw"});
  $("html body").css({
    "overflow-y": "auto"
  })
}

function toggleAccordion(target) {
  if ($(target).parent(".accordion").hasClass("active")) {
    $(target).siblings(".accordion-panel").css({"max-height": 0}).parent(".accordion").removeClass("active");
  } else {
    $(target).siblings(".accordion-panel").animate({"max-height": $(this).height()}).parent(".accordion").addClass("active");
  }
}

function openTab(evt, tabName) {
  $(".tabcontent").hide();
  $(".tablinks").removeClass('active');
  $(`#${tabName}`).css({"display": "block"});
  $(evt.currentTarget).addClass(' active');
}

function openVideo(e, element) {
  e.preventDefault();
  $(element).siblings('.video-player').fadeIn();
  $("body").css({"overflow": "hidden"})
}

function closeVideo(e, element, fromBody) {
  e.preventDefault();
  $("body").css({"overflow": "auto"})
  $(element).parent('.video-player').fadeOut();
  $(element).parent('.video-player').find("video").each(function () { this.pause() });

  if (fromBody) {
    setTimeout(() => {
      $('.video-player').remove();
    }, 200);
  }
}

function popUpVideo(e, element) {
  e.preventDefault();
  var videoCover = $(element).data('cover')
  var videoUrlsArray = $(element).data('video-urls').split(',');


  var renderVideo = `
    <div class="video-player">
      <div class="close-video" onclick="closeVideo(event, this, true)">
        <img src="/static/desktop/images/return.png">
        الرجوع للمباريات
      </div>
      <video webkit-playsinline="true" playsinline="true" id="my-video" class="video-js" controls preload="auto" width="640" height="264" poster="${videoCover}" data-setup="{}">
        ${videoUrlsArray.map(item => {
          return `
            <source src="${item}" type="video/${item.substring(item.lastIndexOf(".") + 1, item.length)}"></source>`
        }).join('')}

        <p class="vjs-no-js">
          To view this video please enable JavaScript, and consider upgrading to a web browser that
          <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
        </p>
      </video>
      <a href="#" class="vf-logo">
        <img src="/static/desktop/images/vodafone-logo.png">
      </a>
    </div>
  `;

  $('body').prepend(renderVideo);

  $('.video-player').fadeIn();
  $("body").css({"overflow": "hidden"})
  console.log(renderVideo);
}