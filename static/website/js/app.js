function toggleAccordion(target) {
	if ($(target).parent(".content").hasClass("active")) {
		$(target).siblings(".content-expand").css({"max-height": 0}).parent(".content").removeClass("active");
	} else {
		$(target).siblings(".content-expand").css({"max-height": $(this).height()}).parent(".content").addClass("active");
	}
}

$(document).ready(function() {
	$('.main-slider .owl-carousel').owlCarousel({
		loop:true,
		items: 1,
		nav:false,
		margin: 0
		// dots: true
	});

	$("#openDefault").click();
});

$(document).on('click', function() {
		$('body').removeClass('menu-open');
	}).on('click', '.menu-btn, .menu-container *', function(e) {
		e.stopPropagation();
	}).on('click', '.menu-btn', function(e) {
		$('body').toggleClass('menu-open');
	});