$(document).ready(function() {
	$('.main-slider .owl-carousel').owlCarousel({
		loop:true,
		items: 1,
		nav:false,
		margin: 0
		// dots: true
	});
});

$(document)
	.on('click', function() {
		$('body').removeClass('menu-open');
	})
	.on('click', '.menu-btn, .menu-container *', function(e) {
		e.stopPropagation();
	})
	.on('click', '.menu-btn', function(e) {
		$('body').toggleClass('menu-open');
	})
