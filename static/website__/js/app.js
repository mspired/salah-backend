$(document).ready(function() {
	$('.main-slider .owl-carousel').owlCarousel({
		autoplay: true,
		autoplayTimeout: 4000,
		loop:true,
		items: 1,
		nav:true,
		margin: 0,
		// dots: true,
		navText: ['<','>']
	});
});

$(document)
	.on('click', function() {
		$('body').removeClass('menu-open');
	})
	.on('click', '.menu-btn, .menu-container *', function(e) {
		e.stopPropagation();
	})
	.on('click', '.menu-btn', function(e) {
		$('body').toggleClass('menu-open');
	})
