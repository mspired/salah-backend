<?php
if(!isset($_GET['v']))
	$_GET['v'] = 'login';
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>MO SALAH - <?= $_GET['v'] ?></title>

		<link rel="icon" href="images/voda-logo.png">

		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="mobile-web-app-capable" content="yes">

		<meta name="mobile-web-app-title" content="MO SALAH">
		<meta name="apple-mobile-web-app-title" content="MO SALAH">
		<link rel="shortcut icon" href="images/voda-logo.png">
		<link rel="apple-touch-icon" href="images/voda-logo.png">
		<link rel="apple-touch-icon-precomposed" href="images/voda-logo.png">

		<!-- Style Links -->
		<link rel="stylesheet" href="stylesheets/style.css" media="all" type="text/css" />
		<!-- End Style Links -->

		<!-- JS Scripts -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script type="text/javascript" src="js/OwlCarousel/owl.carousel.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/app.js"></script>
		<!-- End JS Scripts -->
	</head>
	<body>
		<div class="wrapper">

			<?php include_once('views/partial/menu.php'); ?>

			<div id="content" class="main-container">

				<!-- CONTENT -->
				<?php include_once('views/'.$_GET['v'].'.php'); ?>

			</div>

		</div>

	</body>
</html>
