<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-3.png" alt="">
</div>

<section class="page-content ask-page">
	<div class="container">
		<h3 class="title">اسأل صلاح</h3>
		<form class="ask-form" action="?v=ask" method="post">
			<div class="form-group">
				<input type="text" class="form-control text-right" placeholder="الإسم" name="" value="">
			</div>
			<div class="form-group">
				<textarea name="name" class="form-control text-right" placeholder="نص السؤال"></textarea>
			</div>
			<div class="form-group m-0 text-center">
				<button type="submit" class="btn btn-red" name="button">ابعت سؤالك</button>
			</div>
		</form>
	</div>
</section>
