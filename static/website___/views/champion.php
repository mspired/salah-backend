<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-1.png" alt="">
</div>

<section class="page-content champion-page">
	<div class="container">
		<nav class="nav nav-pills nav-justified">
			<a class="nav-item nav-link active" data-toggle="pill" href="#tab-1">جدول الدوري</a>
			<a class="nav-item nav-link" data-toggle="pill" href="#tab-2">فرق الدوري</a>
			<a class="nav-item nav-link" data-toggle="pill" href="#tab-3">المباريات</a>
		</nav>
		<div class="tab-content">
			<div class="tab-pane fade show active" id="tab-1">
				<h4 class="tab-title">جدول دوري أبطال أوروبا</h4>
				<table class="league-table table">
					<thead>
						<tr>
							<th>pos</th>
							<th></th>
							<th>club</th>
							<th>pld</th>
							<th>pts</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td><span class="stable"></span></td>
							<td>Arsenal</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>2</td>
							<td><span class="up"></span></td>
							<td>Manchester City</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>3</td>
							<td><span class="down"></span></td>
							<td>Liverpool</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>1</td>
							<td><span class="stable"></span></td>
							<td>Arsenal</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>2</td>
							<td><span class="up"></span></td>
							<td>Manchester City</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>3</td>
							<td><span class="down"></span></td>
							<td>Liverpool</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>1</td>
							<td><span class="stable"></span></td>
							<td>Arsenal</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>2</td>
							<td><span class="up"></span></td>
							<td>Manchester City</td>
							<td>20</td>
							<td>45</td>
						</tr>
						<tr>
							<td>3</td>
							<td><span class="down"></span></td>
							<td>Liverpool</td>
							<td>20</td>
							<td>45</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tab-pane fade" id="tab-2">
				<h4 class="tab-title">فرق دوري أبطال أوروبا</h4>
				<div class="teams-table">
					<table class="table">
						<tr>
							<th>Liverpool</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
						</tr>
						<tr>
							<th>Arsenal</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
						</tr>
						<tr>
							<th>Chelsea</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
						</tr>
						<tr>
							<th>Liverpool</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
						</tr>
						<tr>
							<th>Arsenal</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
						</tr>
						<tr>
							<th>Chelsea</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
						</tr>
						<tr>
							<th>Liverpool</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
						</tr>
						<tr>
							<th>Arsenal</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
						</tr>
						<tr>
							<th>Chelsea</th>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="lose">l</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="win">w</span></td>
							<td><span class="draw">d</span></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="tab-pane fade" id="tab-3">
				<h4 class="tab-title">مباريات دوري أبطال أوروبا</h4>

				<table class="league-matches table">
					<tr>
						<th>13:30</th>
						<td>Liverpool - Chelsea</td>
						<td></td>
					</tr>
					<tr>
						<th>13:30</th>
						<td>Liverpool - Chelsea</td>
						<td></td>
					</tr>
					<tr>
						<th>13:30</th>
						<td>Liverpool - Chelsea</td>
						<td></td>
					</tr>
					<tr>
						<th>13:30</th>
						<td>Liverpool - Chelsea</td>
						<td></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</section>
