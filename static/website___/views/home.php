<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-1.png" alt="">
</div>

<section class="page-content home-page">

	<div class="main-slider">

		<div class="container">
			<h3>اخبار مميزة</h3>
			<div class="owl-carousel owl-theme">
				<?php for ($i=0; $i < 4; $i++) { ?>
					<div class="item">
						<a href="?v=videos">
							<img src="images/demo/demo-slider.jpg" alt="">
							<div class="caption has-video">
								<h5>مرحباً بكم في عالم محمد صلاح</h5>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		</div>

	</div>

	<div class="home-sections">
		<div class="container">
			<div class="row m-0">
				<div class="col-6 p-0 pr-2">
					<a href="?v=videos">
						<div class="home-section">
							<h4>احدث الفديوهات</h4>
							<div class="image-container">
								<img src="images/demo/home-section.jpg" alt="">
							</div>
							<p>محمد صلاح بعد آخر يوم تصوير لإعلان فودافون</p>
							<span class="time"> 04 أبريل 2018</span>
						</div>
					</a>
				</div>
				<div class="col-6 p-0 pl-2">
					<a href="?v=news">
						<div class="home-section">
							<h4>احدث الاهداف</h4>
							<div class="image-container">
								<img src="images/demo/home-section.jpg" alt="">
							</div>
							<p>محمد صلاح بعد آخر يوم تصوير لإعلان فودافون</p>
							<span class="time"> 04 أبريل 2018</span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="home-sections news">
		<div class="container">
			<h3 class="section-title">اخر الاخبار</h3>
			<ul class="news-list">
				<?php for ($i=0; $i < 4; $i++) { ?>
					<li>
						<a href="?v=news-details&inside=1">
							<div class="image-container">
								<img src="images/demo/home-section.jpg" alt="">
							</div>

							<div>
								<p>محمد صلاح بعد آخر يوم تصوير لإعلان فودافون</p>
								<span class="time"> 04 أبريل 2018</span>
							</div>
						</a>
					</li>
				<?php } ?>
			</ul>

			<div class="row">
				<div class="next-match">
					<div class="d-flex align-items-center justify-content-between">
						<div class="time">
							<label>المباراة القادمة</label>
							<span>26 مايو 2018</span>
						</div>
						<div class="teams">
							ليفربول / ريال مدريد
						</div>
						<div class="ch-logo">
							<img src="images/demo/ch-league.png" alt="">
						</div>
					</div>
				</div>

				<div class="countdown" data-time="">
					<span class="s">30 ثانيه</span> <span class="m">30 دقيقة</span> <span class="h">10 ساعة</span> <span>30 يوم</span>
				</div>
			</div>
		</div>
	</div>

	<div class="salah-numbers">
		<div class="container">
			<div class="in-row row">
				<div class="numbers-content">
					<h4>أرقام محمد صلاح</h4>
					<table class="table table-bordered">
						<tr>
							<td>
								50
								<span>عرضيات</span>
							</td>
							<td>
								50
								<span>عرضيات</span>
							</td>
							<td>
								50
								<span>عرضيات</span>
							</td>
						</tr>
						<tr>
							<td>
								50
								<span>عرضيات</span>
							</td>
							<td>
								50
								<span>عرضيات</span>
							</td>
							<td>
								50
								<span>عرضيات</span>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

</section>
