<?php include_once('views/partial/header.php'); ?>


<div class="header-caption container with-content no-image">
	<div class="status-board">
		<div class="d-flex justify-content-center align-items-center">
			<span>ليفربول</span> <span class="result"><span>2</span> - <span class="text-muted">0</span></span> <span>ارسنال</span>
		</div>
	</div>
</div>

<div class="match-details page-content with-bg">
	<div class="container">
		<img src="images/demo/match-details.jpg" class="img-fluid mb-3" alt="">

		<div class="en">
			<p class="font-weight-bold">Edin Dzeko and Mohamed Salah missed crucial chances for Luciano Spalletti's side and they were made to pay as the Blancos eventually cruised into the Champions League last-eight</p>

			<p>Goals from Cristiano Ronaldo and James Rodrguez eased Real Madrid into the quarter-finals of the Champions League despite a difficult night against Roma.</p>
			<p>Madrid's chances of domestic glory have taken a major hit since they won at the Stadio Olimpico on February 17, leaving the Champions League as their best hope of silverware this season</p>
			<p>And, after a first half in which Madrid attempted 18 shots without success, Ronaldo – scorer of four goals in a 7-1 victory over Celta Vigo on Saturday – broke the deadlock, before teeing up James for the second.</p>
			<p>Goals from Cristiano Ronaldo and James Rodrguez eased Real Madrid into the quarter-finals of the Champions League despite a difficult night against Roma.</p>
			<p>Madrid's chances of domestic glory have taken a major hit since they won at the Stadio Olimpico on February 17, leaving the Champions League as their best hope of silverware this season</p>
			<p>And, after a first half in which Madrid attempted 18 shots without success, Ronaldo – scorer of four goals in a 7-1 victory over Celta Vigo on Saturday – broke the deadlock, before teeing up James for the second.</p>
		</div>
	</div>
</div>
