<?php include_once('views/partial/header.php'); ?>


<div class="header-caption container">
	<img class="small" src="images/header/salah-3.png" alt="">
	<div class="status-board with-bg">
		<div class="d-flex justify-content-center align-items-center">
			<span>ليفربول</span> <span class="result"><span>2</span> - <span class="text-muted">0</span></span> <span>ارسنال</span>
		</div>
	</div>
</div>

<div class="match-live page-content">
	<div class="container">
		<ul class="details-list">
			<li>
				<div class="row">
					<div class="col-2 p-0 per-minute">
						<span>72'</span>
						<span class="icon goal"></span>
					</div>
					<div class="col-10 pl-0">
						<p>He's missed a sitter! Oh my, you can't make that up. Lucas plays Ronaldo clean through on goal, and he has half the goal to aim at with Szczesny scrambling to get into position, but somehow rolls his finish beyond the far post!</p>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-2 p-0 per-minute">
						<span>71'</span>
						<span class="icon penalty"></span>
					</div>
					<div class="col-10 pl-0">
						<p>He's missed a sitter! Oh my, you can't make that up. Lucas plays Ronaldo clean through on goal, and he has half the goal to aim at with Szczesny scrambling to get into position, but somehow rolls his finish beyond the far post!</p>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-2 p-0 per-minute">
						<span>71'</span>
						<span class="icon yellow-card"></span>
					</div>
					<div class="col-10 pl-0">
						<p class="font-weight-bold">He's missed a sitter! Oh my, you can't make that up. Lucas plays Ronaldo clean through on goal, and he has half the goal to aim at with Szczesny scrambling to get into position, but somehow rolls his finish beyond the far post!</p>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-2 p-0 per-minute">
						<span>66'</span>
						<span class="icon in-out"></span>
					</div>
					<div class="col-10 pl-0">
						<p>A standing ovation at the Santiago Bernabeu - a wonderful sign of respect from these Madrid fans - as <span class="text-success">Francesco Totti</span> comes on for what is likely his Champions League swansong. It's not how he'll have envisioned going out, but it's a deserved cameo for the long-time Giallorossi forward. <span class="text-danger">El Shaarawy</span> gives way.</p>
					</div>
				</div>
			</li>
			<li>
				<div class="row">
					<div class="col-2 p-0 per-minute">
						<span>68</span>
						<span class="icon red-card"></span>
					</div>
					<div class="col-10 pl-0">
						<p>He's missed a sitter! Oh my, you can't make that up. Lucas plays Ronaldo clean through on goal, and he has half the goal to aim at with Szczesny scrambling to get into position, but somehow rolls his finish beyond the far post!</p>
					</div>
				</div>
			</li>
		</div>
	</div>
</div>
