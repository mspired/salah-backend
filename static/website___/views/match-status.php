<?php include_once('views/partial/header.php'); ?>

<div class="match-status">
	<div class="container">

		<div class="status-board">
			<div class="row align-items-center">
				<div class="col p-0 text-right">
					ليفربول
				</div>
				<div class="col text-center">
					<span>2</span> - <span class="text-muted">0</span>
				</div>
				<div class="col p-0 text-left">
					ارسنال
				</div>
			</div>
		</div>
		<div>
			<table class="progress-status">
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-center">ASSISTS</td>
					<td><span class="progress left" style="width: 0%">0</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 80%">8</span></td>
					<td class="text-center">FREE KICKS</td>
					<td><span class="progress left green" style="width: 30%">4</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 10%">1</span></td>
					<td class="text-center">PENALTIES</td>
					<td><span class="progress left green" style="width: 10%">1</span></td>
				</tr>
			</table>

			<table class="progress-status">
				<tr>
					<td><span class="progress right blue" style="width: 90%">24</span></td>
					<td class="text-center">TOTAL SHOTS</td>
					<td><span class="progress left green" style="width: 50%">9</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 60%">14</span></td>
					<td class="text-center">SHOTS ON TARGET</td>
					<td><span class="progress left green" style="width: 30%">4</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 50%">10</span></td>
					<td class="text-center">SHOTS OFF TARGET</td>
					<td><span class="progress left green" style="width: 40%">6</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-center">CORNERS</td>
					<td><span class="progress left green" style="width: 30%">4</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-center">SAVES</td>
					<td><span class="progress left green" style="width: 80%">11</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-center">OFFSIDES</td>
					<td><span class="progress left green" style="width: 10%">3</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-center">FOULS</td>
					<td><span class="progress left green" style="width: 30%">8</span></td>
				</tr>
				<tr>
					<td><span class="progress right" style="">0</span></td>
					<td class="text-center">YELLOW CARDS</td>
					<td><span class="progress left green" style="width: 30%">4</span></td>
				</tr>
				<tr>
					<td><span class="progress right" style="">0</span></td>
					<td class="text-center">RED CARDS</td>
					<td><span class="progress left green" style="width: 10%">1</span></td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-center">PENALTIES</td>
					<td><span class="progress left green" style="width: 30%">4</span></td>
				</tr>
			</table>
		</div>
	</div>
</div>
