<?php include_once('views/partial/header.php'); ?>


<div class="header-caption container with-content no-image">
	<div class="status-board">
		<div class="d-flex justify-content-center align-items-center">
			<span>ليفربول</span> <span class="result"><span>2</span> - <span class="text-muted">0</span></span> <span>ارسنال</span>
		</div>
	</div>
</div>

<div class="match-timeline page-content with-bg">
	<div class="container">
		<ul class="timeline">
			<li class="left"> <!-- Goal -->
				<div class="line-icon">
					<span class="icon goal"></span>
				</div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="caption">
						<span class="text-success">Goal</span>
						<label>Andres InIniesta</label>
					</div>
					<div class="time">
						<span class="text-primary">68'</span>
					</div>
				</div>
			</li>
			<li class="right"> <!-- Penalty -->
				<div class="line-icon">
					<span class="icon penalty"></span>
				</div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="time">
						<span class="text-muted">70'</span>
					</div>
					<div class="caption">
						<span class="text-success">Penalty</span>
						<label>Deulofeu</label>
					</div>
				</div>
			</li>
			<li class="left">  <!-- Goal -->
				<div class="line-icon">
					<span class="icon goal"></span>
				</div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="caption">
						<span class="text-success">Goal</span>
						<label>Leo Messi</label>
					</div>
					<div class="time">
						<span class="text-primary">68'</span>
					</div>
				</div>
			</li>
			<li class="right"> <!-- All -->
				<div class="line-icon"><span></span></div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="time">
						<span class="text-muted">83'</span>
					</div>
					<div class="caption">
						<span class="icon in-out"></span>
						<label>Ross Barkley</label>
					</div>
				</div>
			</li>
			<li class="right"> <!-- All -->
				<div class="line-icon"><span></span></div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="time">
						<span class="text-muted">83'</span>
					</div>
					<div class="caption">
						<span class="icon yellow-card"></span>
						<label>Ross Barkley</label>
					</div>
				</div>
			</li>
			<li class="right"> <!-- All -->
				<div class="line-icon"><span></span></div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="time">
						<span class="text-muted">83'</span>
					</div>
					<div class="caption">
						<span class="icon red-card"></span>
						<label>Ross Barkley</label>
					</div>
				</div>
			</li>
			<li class="right"> <!-- All -->
				<div class="line-icon"><span></span></div>
				<div class="line-content d-flex justify-content-center align-items-center">
					<div class="time">
						<span class="text-primary">90'+5</span>
					</div>
					<div class="caption">
						<span class="icon"></span>
						<label>Extra Time</label>
					</div>
				</div>
			</li>
		</ul>
	</div>
</div>
