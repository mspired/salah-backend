<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-2.png" alt="">

	<h2>اخبار <br> محمد صلاح</h2>
</div>

<section class="page-content news-page">
	<div class="container">
		<ul class="news-list">
			<?php for ($i=0; $i < 4; $i++) { ?>
				<li>
					<a href="?v=news-details&inside=1">
						<div class="image-container">
							<img src="images/demo/home-section.jpg" alt="">
						</div>

						<div>
							<p>محمد صلاح بعد آخر يوم تصوير لإعلان فودافون</p>
							<span class="time"> 04 أبريل 2018</span>
						</div>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</section>
