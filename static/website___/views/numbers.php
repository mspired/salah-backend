<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container with-content">
	<img class="small" src="images/header/salah-4.png" alt="">
	<h3>ارقام محمد صلاح</h3>

	<nav class="nav nav-pills nav-justified">
		<a class="nav-item nav-link active" data-toggle="pill" href="#tab-1">منتخب مصر</a>
		<a class="nav-item nav-link" data-toggle="pill" href="#tab-2">أبطال أوروبا</a>
		<a class="nav-item nav-link" data-toggle="pill" href="#tab-3">البريميرليج</a>
	</nav>
</div>

<section class="page-content numbers-page">
	<div class="tab-content">
		<div class="tab-pane fade show active" id="tab-1">
			<table class="table">
				<tr>
					<th class="text-center" colspan="2">المباريات</th>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>

				<tr>
					<th class="text-center" colspan="2">الأهداف</th>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
			</table>
		</div>
		<div class="tab-pane fade" id="tab-2">
			<table class="table">
				<tr>
					<th class="text-center" colspan="2">المباريات</th>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>

				<tr>
					<th class="text-center" colspan="2">الأهداف</th>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
			</table>
		</div>
		<div class="tab-pane fade" id="tab-3">
			<table class="table">
				<tr>
					<th class="text-center" colspan="2">المباريات</th>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>

				<tr>
					<th class="text-center" colspan="2">الأهداف</th>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
				<tr>
					<td class="text-right">(فوز 14 - 11 تعادل)</td>
					<td class="text-left"> : عدد المباريات</td>
				</tr>
				<tr>
					<td class="text-right">32</td>
					<td class="text-left"> : اساسي</td>
				</tr>
				<tr>
					<td class="text-right">2</td>
					<td class="text-left"> : بديل</td>
				</tr>
			</table>
		</div>
	</div>

</section>
