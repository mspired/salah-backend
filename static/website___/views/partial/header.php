<header class="fixed-top">
	<div class="container clearfix">
		<?php if(!isset($_GET['inside'])) { ?>
			<button class="menu-btn" type="button">
				<div class="bar1"></div> <div class="bar2"></div> <div class="bar3"></div>
			</button>
		<?php } else { ?>
			<a class="return" href="?v=home"></a>
			<span class="return-title">الرجوع المباريات</span>
		<?php } ?>
		<div class="v-logo">
			<img src="images/voda-logo.png" alt="">
		</div>
	</div>
</header>
