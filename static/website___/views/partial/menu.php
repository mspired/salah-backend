<div class="menu-container">
	<div class="d-flex flex-column">
		<div class="user mb-auto">
			<h3>Tamer Azab</h3>
			<a href="?v=login">Log Out</a>
		</div>

		<div class="menu">
			<ul>
				<li><a href="?v=home">الرئيسية</a></li>
				<li><a href="?v=videos">الفيديوهات</a></li>
				<li><a href="?v=news">أخر الاخبار</a></li>
				<li><a href="?v=numbers">ارقام محمد صلاح</a></li>
				<li><a href="?v=champion">المسبقات</a></li>
				<li><a href="?v=ask">اسأل محمد صلاح</a></li>
				<li><a href="?v=life">محمد صلاح مباشر</a></li>
				<li><a href="?v=terms">الشروط والاحكام</a></li>
			</ul>
		</div>

		<div class="social mt-auto">
			<ul>
				<li><a href="#" class="facebook"></a></li>
				<li><a href="#" class="google"></a></li>
				<li><a href="#" class="twitter"></a></li>
			</ul>
		</div>
	</div>
</div>
