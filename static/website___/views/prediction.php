<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-3.png" alt="">
</div>

<section class="page-content prediction-page">
	<div class="container">
		<h3 class="title">التوقعات</h3>

		<form class="" action="?v=prediction" method="post">
			<div class="row">
				<div class="col">
					<h4>ارسنال</h4>
					<div class="form-group">
						<input type="number" class="form-control" name="" value="">
					</div>
				</div>
				<div class="col">
					<h4>ليفربول</h4>
					<div class="form-group">
						<input type="number" class="form-control" name="" value="">
					</div>
				</div>
			</div>
			<div class="form-group m-0 text-center">
				<button type="submit" class="btn btn-red" name="button">ارسل توقعك</button>
			</div>
		</form>
	</div>
</section>
