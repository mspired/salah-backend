<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container with-bg">
	<h2 class="center">ارقام صلاح</h2>
</div>

<div class="salah-status page-content">
	<div class="container">

		<div>
			<table class="progress-status">
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">ASSISTS</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 80%">8</span></td>
					<td class="text-left">FREE KICKS</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 10%">1</span></td>
					<td class="text-left">PENALTIES</td>
				</tr>
			</table>

			<table class="progress-status">
				<tr>
					<td><span class="progress right blue" style="width: 90%">24</span></td>
					<td class="text-left">TOTAL SHOTS</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 50%">14</span></td>
					<td class="text-left">SHOTS ON TARGET</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">SHOTS OFF TARGET</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">CORNERS</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">SAVES</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">OFFSIDES</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">FOULS</td>
				</tr>
				<tr>
					<td><span class="progress right" style="">0</span></td>
					<td class="text-left">YELLOW CARDS</td>
				</tr>
				<tr>
					<td><span class="progress right" style="">0</span></td>
					<td class="text-left">RED CARDS</td>
				</tr>
				<tr>
					<td><span class="progress right blue" style="width: 30%">4</span></td>
					<td class="text-left">PENALTIES</td>
				</tr>
			</table>
		</div>
	</div>
</div>
