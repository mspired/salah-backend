<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-1.png" alt="">
</div>

<section class="page-content with-bg trophies-page">
	<div class="container">
		<h2 class="title">جوائز محمد صلاح</h2>
		<div class="trophies-list row">
			<?php for ($i=0; $i < 3; $i++) { ?>
				<div class="col-6">
					<div class="">
						<div class="img-container">
							<img src="images/demo/trophies-1.jpg" alt="">
						</div>
						<h4>افضل لاعب في انجلتر</h4>
					</div>
				</div>
				<div class="col-6">
					<div class="">
						<div class="img-container">
							<img src="images/demo/trophies-2.jpg" alt="">
						</div>
						<h4>افضل لاعب في انجلتر</h4>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
