<?php include_once('views/partial/header.php'); ?>

<link href="https://vjs.zencdn.net/7.0.3/video-js.css" rel="stylesheet">
<!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
<script src="http://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>


<div class="video-player">
	<video id="video-player" class="video-js" controls preload="auto" width="100vw" height="100vh" poster="images/demo/news-details.jpg" data-setup="{}">
		<source src="//vjs.zencdn.net/v/oceans.mp4" type="video/mp4"></source>
		<source src="//vjs.zencdn.net/v/oceans.webm" type="video/webm"></source>
		<source src="//vjs.zencdn.net/v/oceans.ogv" type="video/ogg"></source>
		<p class="vjs-no-js">
			To view this video please enable JavaScript, and consider upgrading to a web browser that
			<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
		</p>
	</video>
	<script src="https://vjs.zencdn.net/7.0.3/video.js"></script>
</div>

<script type="text/javascript">
	// var options = {
	//
	// };
	// var player = videojs('video-player', options);
</script>
