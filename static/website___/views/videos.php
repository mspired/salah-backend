<?php include_once('views/partial/header.php'); ?>

<div class="header-caption container">
	<img class="big" src="images/header/salah-2.png" alt="">

	<h2>فيديوهات <br> محمد صلاح</h2>
</div>

<section class="page-content videos-page">
	<div class="container">
		<div class="videos-list">
			<div class="row">
				<?php for ($i=0; $i < 6; $i++) { ?>
					<div class="col-6">
						<a href="?v=video-play&inside=1">
							<div class="image-container">
								<img src="images/demo/home-section.jpg" alt="">
							</div>

							<div class="caption">
								<p>محمد صلاح بعد آخر يوم تصوير لإعلان فودافون</p>
							</div>
						</a>
					</div>
				<?php } ?>
			</div>
		</ul>
	</div>
</section>
