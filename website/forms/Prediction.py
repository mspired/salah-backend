from backend.models import Prediction

__author__ = 'ashraf'

from django import forms


class PredictionForm(forms.ModelForm):
    class Meta:
        model = Prediction
        fields = ['id', 'score']
