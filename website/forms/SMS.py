from django import forms


class SMSForm(forms.Form):
    code = forms.CharField(label='SMS Verification Code')