from django.contrib.auth.views import logout
from django.urls import path

from backend import views as backend_views
from desktop.views.liverpool import LiverpoolVideoListExternal
from salah import settings
from website.views import *
from website.views.career import CareerList
from website.views.gallery import GalleryList, GalleryDetails
from website.views.liverpool import LiverpoolVideoList, LiverpoolVideoDetails
from website.views.signup import signup, Welcome, sms_verification
from website.views.social import social
from website.views.ask import QuestionCreate
from website.views.champion import TeamsList
from website.views.home import index
from website.views.login import login_user, no_credit, forget_password
from website.views.news import NewsList, NewsDetails
from website.views.numbers import NumbersList, MatchNumbers
from website.views.prediction import predict
from website.views.salah_status import status
from website.views.terms import Terms, TermsView
from website.views.trophy import AwardsList
from website.views.videos import VideoList, VideoDetails
from website.views.ana_vf import subscribe_by_id
__author__ = 'ashraf'

from django.conf.urls import url, include


# Routers provide an easy way of automatically determining the URL conf.
# router = routers.DefaultRouter()
# router.register(r'profile', views.UserProfileViewSet)

# profiles_list = UserProfileViewSet.as_view({
#     'get': 'list'
# })

urlpatterns = [
    path('login', login_user, name='login'),
    # path('loginapi', LoginFromAPI, name='login-api'),
    path('logout', logout, {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    path('signup', signup, name='signup'),
    path('verify', sms_verification, name='sms'),
    path('welcome', Welcome.as_view(), name='welcome'),
    path('home', index, name='home'),
    path('', index, name='home'),
    # News
    path('news/', NewsList.as_view(), name='news-list'),
    path('news/<int:pk>/', NewsDetails.as_view(), name='news-details'),
    # Numbers
    path('numbers/', NumbersList.as_view(), name='numbers-list'),
    # Champion
    path('champions/', TeamsList.as_view(), name='champions-list'),
    # Videos
    path('videos/', VideoList.as_view(), name='videos-list'),
    path('videos/<int:pk>/', VideoDetails.as_view(), name='video-details'),
    path('Vodafone/<int:pk>/', VideoDetails.as_view(), name='video-details'),
    # Gallery
    path('gallery/', GalleryList.as_view(), name='gallery-list'),
    path('gallery/<int:pk>/', GalleryDetails.as_view(), name='gallery-details'),
    # Questions (ASK)
    path('ask/', QuestionCreate.as_view(), name='question-add'),
    # Prediction
    path('predict/', predict, name='predict-add'),
    # Salah-Status (Not Configured Yet)
    path('status/', status, name='status-show'),
    # Social (Not Configured Yet)
    path('social/', social, name='social-show'),
    # Liverpool page
    # path('salah-liverpool/', LiverpoolVideoList.as_view(), name='liverpool-videos-list'),
    path('salah-liverpool/', LiverpoolVideoListExternal.as_view(), name='liverpool-videos-list'),
    path('salah-liverpool/<int:pk>/', LiverpoolVideoDetails.as_view(), name='liverpool-video-details'),
    # Terms
    path('terms/', TermsView.as_view(), name='terms'),

    path('awards/', AwardsList.as_view(), name='awards-list'),
    path('career/', CareerList.as_view(), name='career-list'),

    path('match-stats/', MatchNumbers, name='match-stats'),
    path('no-credit/', no_credit, name='no-credit'),
    path('forget-password/', forget_password, name='forget-password'),
    path('ana-vf/', subscribe_by_id, name='subscribe_by_id')
]
