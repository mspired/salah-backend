from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView
from django.shortcuts import redirect

from backend.models import AskQuestion
from salah import parameters
from salah_admin.views.shared import check_validity, check_user

import base64


# @method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class QuestionCreate(CreateView):
    model = AskQuestion
    fields = ('title', 'question')
    template_name = 'website/views/ask.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        audio_file = self.request.FILES.get('question')
        shipphoto_obj = AskQuestion.objects.create(user=self.request.user,
        title='{} - {}'.format(form.cleaned_data['title'], self.request.user.username),
        question=audio_file)

        # audio_file = self.request.FILES.get('question')
        # shipphoto_obj = AskQuestion.objects.create(user=User.objects.first(),
        #                                            title='{} - {}'.format('lakskl', 'okokok'),
        #                                            question=audio_file)
        self.request.session['success'] = True
        if self.request.is_ajax():

            return JsonResponse({
            'success': True,
        })
        return redirect('home')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(QuestionCreate, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')

    def get_success_url(self):
        messages.success(self.request, 'تم حفظ سؤالك بنجاح .. شكرا لك')
        return super().get_success_url()

