from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.shortcuts import redirect

from backend.models import Teams, Match, MatchTeams, LeaguesTeams
from salah import parameters
from salah_admin.views.shared import check_validity, check_user


# @method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class TeamsList(ListView):
    model = Teams
    queryset = LeaguesTeams.objects.all().order_by('rank')
    context_object_name = 'teams_objects'
    template_name = 'website/views/champion.html'

    def get_context_data(self, **kwargs):
        context = super(TeamsList, self).get_context_data(**kwargs)
        context['matches'] = Match.objects.all().order_by('start_date')
        matchTeams = MatchTeams.objects.all()
        teamsResults = {}
        for mt in matchTeams:
            if mt.team.title in teamsResults:
                teamsResults[mt.team.title].append(mt.result)
            else:
                teamsResults[mt.team.title] = [mt.result]
        context['matches_teams'] = teamsResults
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')

