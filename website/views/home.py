import pytz
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django_user_agents.utils import get_user_agent

from backend.models import Slider, News, Videos, Match, SalahNumbers, SalahLeague, MatchTimeline, Teams, MatchTeams
import datetime

from salah import parameters
from salah_admin.views.shared import check_validity, check_user


# @login_required(login_url='login', redirect_field_name='next')
def index(request):
    user_agent = get_user_agent(request)

    if user_agent.is_pc:
        return redirect('desktop-home')

    if user_agent.is_pc:
        return redirect('desktop-videos')

    if check_validity(request):
        if check_validity(request) == 'failed':
            return redirect('login')
        if check_user(request) == '0':
            # load slider
            items = Slider.objects.all()
            # recent news and videos
            last_news = News.objects.order_by('-id').all()[:5]
            last_video = Videos.objects.order_by('id').last()
            # latest match
            tz = pytz.timezone('EET')
            next_match = None
            time_now = datetime.datetime.now(tz=tz) - datetime.timedelta(hours=1)
            if Match.objects.filter(status='In-Progress').count() > 0:
                next_match = Match.objects.filter(status='In-Progress').order_by('start_date')[0]
            else:
                next_match = Match.objects.filter(start_date__gte=time_now).order_by('start_date')[0]
            next_match_teams = []
            league = SalahLeague.objects.get(title=next_match.league)
            salah_numbers = SalahNumbers.objects.filter(league=league)

            for team in next_match.teams.all()[:2]:
                t = MatchTeams.objects.get(team=team, match=next_match)

                next_match_teams.append(team)
                next_match_teams.append(t)

            # counter
            tz = pytz.timezone('EET')

            time_match = next_match.start_date
            diff = time_match - time_now
            hours = diff.seconds // 3600
            seconds = diff.seconds - (hours * 3600)
            minutes = seconds // 60
            seconds = seconds - (minutes * 60)
            count_down = {'days': diff.days, 'hours': hours, 'minutes': minutes, 'seconds': seconds}
            last_match_timeline = None
            if next_match.status == 'In-Progress':
                last_match_timeline = MatchTimeline.objects.filter(match=next_match, type='GOAL').order_by('-id')[:6]

            context = {'items': items, 'news': last_news, 'video': last_video, 'next_match_date': next_match.start_date,
                       'next_match': next_match, 'next_match_teams': next_match_teams, 'next_match_league': league,
                       'salah_numbers': salah_numbers, 'count_down': count_down,
                       'image_host': parameters.IMAGE_URL_HOST, 'last_match_timeline': last_match_timeline}
            if request.session.get('success', None):
                context['ask_success'] = True
                del request.session['success']
            return render(request, 'website/views/home.html', context=context)
        else:
            messages.error(request, 'برجاء الإشتراك فى الخدمة')
            return redirect('login')
    return redirect('sms')

