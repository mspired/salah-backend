from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView

from backend.models import Videos
from salah import parameters
from salah_admin.views.shared import check_validity, check_user


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class LiverpoolVideoList(ListView):
    queryset = Videos.objects.filter(video_type='view2').order_by('-id')
    # template_name = 'website/views/videos_liverpool.html'
    template_name = 'lfctv/rtl.html'
    # context_object_name = 'videos_list'
    context_object_name = 'liverpool_videos_list'

    def get_context_data(self, **kwargs):
        context = super(LiverpoolVideoList, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        context['redirect_video'] = 'https://www.liverpoolfc.com/Vodafone'
        return context

    def dispatch(self, *args, **kwargs):
        from django_user_agents.utils import get_user_agent
        user_agent = get_user_agent(self.request)

        if user_agent.is_pc:
            last_vid = Videos.objects.all().order_by('-id').first()
            return redirect('desktop-liverpool', last_vid.id)
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class LiverpoolVideoDetails(DetailView):
    model = Videos
    template_name = 'lfctv/video_details.html'
    context_object_name = 'video'

    def get_context_data(self, **kwargs):
        context = super(LiverpoolVideoDetails, self).get_context_data(**kwargs)
        context['video_host'] = parameters.VIDEO_URL_HOST
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['liverpool_videos_list'] = Videos.objects.filter(video_type='view2').order_by('-id')[:4]
        return context

    def dispatch(self, *args, **kwargs):
        from django_user_agents.utils import get_user_agent
        user_agent = get_user_agent(self.request)

        if user_agent.is_pc:
            last_vid = Videos.objects.all().order_by('-id').first()
            return redirect('desktop-liverpool', last_vid.id)
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')
