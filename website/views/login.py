import json
import urllib.request
from random import randint

import xmltodict
import requests
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django_user_agents.utils import get_user_agent

from salah import settings
import time
from lxml import html, etree
from django.contrib.auth import authenticate, login, logout

from salah_admin.views.shared import check_header


def login_user(request):
    user_agent = get_user_agent(request)

    if user_agent.is_pc:
        return redirect('desktop-login')
    username = password = ''

    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user'
        response = requests.get(url, params={'phone': request.POST.get('username')})
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        if response.status_code != 200:
            return redirect('login')
        else:
            tree = etree.fromstring(response.content, parser=parser)
            response = tree.xpath("/Response/StatusCode")
            response_result = response[0].text
            if response_result == '0':
                user = authenticate(username=username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect('home')
                else:
                    messages.error(request, 'رقم الهاتف او كلمة المرور غير صحيحه')
                    return redirect('login')
                    # return HttpResponse(response_result)
            elif response_result == '-9':
                messages.error(request, 'برجاء الاشتراك فى الخدمة')
                return redirect('signup')
            elif response_result == '1':
                return redirect('no-credit')
            messages.error(request, 'رقم الهاتف او كلمة المرور غير صحيحه')
            return redirect('login')
    else:
        if request.META.get('HTTP_X_VF_MSISDN', None):

            check_header(request)



        return render(request, 'website/views/login.html')



def no_credit(request):
    return render(request, 'website/views/no_credit.html')


def forget_password(request):
    if request.method == 'POST':
        user = None
        try:
            user = User.objects.get(username=request.POST.get('username'))
        except:
            user = None
        if user:
            sms_url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/send-sms'
            rand = randint(100000, 999999)  # randint is inclusive at both ends
            message_to_send = requests.post(sms_url, data={'phone': request.POST.get('username'),
            'smstext': 'Please use this password for Mohamed Salah Portal in case of WIFI login: {}'.format(str(rand))})
            if message_to_send.status_code != 200:
                return redirect('login')
            else:
                user = User.objects.get(username=request.POST.get('username'))
                user.set_password(str(rand))
                user.save()
        return redirect('login')
    else:
        return render(request, 'website/views/forget_password.html')
    # not working
def loginaaaaaaaaa(request):
    if request.method == 'POST':
        url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user'
        response = requests.get(url, params={'phone': request.POST.get('username')})
        parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')
        if response.status_code != 200:
            return redirect('login')
        else:
            tree = etree.fromstring(response.content, parser=parser)
            response = tree.xpath("/Response/StatusCode")
            response_result = response[0].text
            if response_result == '0':
                if request.session['first']:
                    return redirect('welcome')
                else:
                    return redirect('home')
            else:
                return ('signup')
    return render(request, 'website/views/login.html')

def LoginFromAPI(request):
    url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/check-user/'
    # url = 'http://10.0.0.26/APIs/Mspired/wbVFMspiredSalahPortal/send-sms'
    # url = 'https://jsonplaceholder.typicode.com/posts/'
    response = ''
    response = requests.get(url, params={'phone': 1002228492})
    # response = requests.post(url, data={'phone': '1095583881', 'smstext': 'ashraf emad'})
    return render(request, 'website/views/login.html')