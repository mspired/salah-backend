import datetime

import pytz
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, TemplateView

from backend.models import SalahNumbers, SalahLeague, Match, MatchTeams, MatchDetails, MatchLive, MatchTimeline, \
    MatchPitch
from salah import parameters
from salah_admin.views.shared import check_validity, check_user
from django.shortcuts import redirect, render


# @method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class NumbersList(ListView):
    model = SalahNumbers
    queryset = SalahNumbers.objects.all().order_by('id')
    context_object_name = 'numbers_objects'
    template_name = 'website/views/numbers.html'

    def get_context_data(self, **kwargs):
        context = super(NumbersList, self).get_context_data(**kwargs)
        context['first_league'] = SalahLeague.objects.values('id', 'title').first()
        context['leagues'] = SalahLeague.objects.exclude(id=context['first_league']['id'])
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        from django_user_agents.utils import get_user_agent
        user_agent = get_user_agent(self.request)

        if user_agent.is_pc:
            return redirect('desktop-salah-numbers')
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')


@login_required(login_url='login', redirect_field_name='next')

def MatchNumbers(request):
    from django_user_agents.utils import get_user_agent
    user_agent = get_user_agent(request)

    if user_agent.is_pc:
        return redirect('desktop-match-stats')
    tz = pytz.timezone('EET')

    time_now = datetime.datetime.now(tz=tz) - datetime.timedelta(hours=1)
    next_match = None
    if Match.objects.filter(status='In-Progress').count() > 0:
        next_match = Match.objects.filter(status='In-Progress').order_by('start_date')[0]
    else:
        next_match = Match.objects.filter(start_date__gte=time_now).order_by('start_date')[0]
    team1 = MatchTeams.objects.values().get(match=next_match, team=next_match.teams.all()[0])
    team2 = MatchTeams.objects.values().get(match=next_match, team=next_match.teams.all()[1])
    team1['name'] = next_match.teams.all()[0]
    team2['name'] = next_match.teams.all()[1]
    match_details = None
    match_live = None
    match_timeline = None
    match_pitch = None
    try:
        match_details = MatchDetails.objects.get(match=next_match)
    except:
        pass
    try:

        match_live = MatchLive.objects.filter(match=next_match).order_by('-minute')
    except:
        pass
    try:
        match_timeline = MatchTimeline.objects.filter(match=next_match).order_by('minute')
    except:
        pass
    try:
        match_pitch = MatchPitch.objects.filter(match=next_match).order_by('id').last()
        keeper_name = match_pitch.goal_keeper_name
        keeper_number = match_pitch.goal_keeper_number
        defenders = {}
        for i in match_pitch.defenders_names.split('-'):
            defenders[i] = match_pitch.defenders_numbers.split('-')[match_pitch.defenders_names.split('-').index(i)]

        mid = {}
        for i in match_pitch.mid_names.split('-'):
            mid[i] = match_pitch.mid_numbers.split('-')[match_pitch.mid_names.split('-').index(i)]

        attack = {}
        for i in match_pitch.attack_names.split('-'):
            attack[i] = match_pitch.attack_numbers.split('-')[match_pitch.attack_names.split('-').index(i)]

        match_pitch = {'keeper_name': keeper_name, 'keeper_number': keeper_number, 'defenders': defenders, 'mid': mid, 'attack': attack}
    except:
        pass

    return render(request, 'website/views/match-status.html', {'image_host': parameters.IMAGE_URL_HOST, 'team1': team1, 'team2': team2, 'match_details': match_details, 'match_live': match_live, 'match_timeline': match_timeline, 'match_pitch': match_pitch, 'next_match': next_match})