from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from salah import parameters

@login_required(login_url='login', redirect_field_name='next')

def status(request):
    return render(request, 'website/views/salah-status.html', {'image_host': parameters.IMAGE_URL_HOST})