from django.views.generic import TemplateView

from backend.models import Terms
from salah import parameters


class TermsView(TemplateView):
    template_name = 'website/views/terms.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(TermsView, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['terms'] = Terms.objects.get(id=1)
        return context