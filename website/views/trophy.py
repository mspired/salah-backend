from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView

from backend.models import Career, Awards
from salah import parameters
from salah_admin.views.shared import check_validity, check_user
from django.shortcuts import redirect


# @method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class AwardsList(ListView):
    queryset = Awards.objects.all().order_by('-year')
    template_name = 'website/views/trophies.html'
    context_object_name = 'trophy_list'

    def get_context_data(self, **kwargs):
        context = super(AwardsList, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        from django_user_agents.utils import get_user_agent
        user_agent = get_user_agent(self.request)

        if user_agent.is_pc:
            return redirect('desktop-career')
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')
