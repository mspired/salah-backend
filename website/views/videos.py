from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django_user_agents.utils import get_user_agent

from backend.models import News, Videos
from salah import parameters
from salah_admin.views.shared import check_validity, check_user
from django.shortcuts import redirect


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class VideoList(ListView):
    queryset = Videos.objects.filter(video_type='view1').order_by('-created_at')
    template_name = 'website/views/videos.html'
    context_object_name = 'videos_list'
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super(VideoList, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['liverpool_videos'] = Videos.objects.filter(video_type='view2').order_by('-created_at')[:40]
        return context

    def dispatch(self, *args, **kwargs):
        user_agent = get_user_agent(self.request)

        if user_agent.is_pc:
            return redirect('desktop-videos')
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            elif check_user(self.request) == '-9':
                messages.error(self.request, 'انت غير مشترك بالخدمة')
                return redirect('login')
            elif check_user(self.request) == '1':
                messages.error(self.request, 'لا يوجد رصيد كافى, برجاء الشحن وإعادة المحاولة')
                return redirect('login')
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')


@method_decorator(login_required(login_url='login', redirect_field_name='next'), name='dispatch')
class VideoDetails(DetailView):
    model = Videos
    template_name = 'website/views/video-play.html'
    context_object_name = 'video_object'

    def get_context_data(self, **kwargs):
        context = super(VideoDetails, self).get_context_data(**kwargs)
        context['image_host'] = parameters.IMAGE_URL_HOST
        context['video_host'] = parameters.VIDEO_URL_HOST
        return context

    def dispatch(self, *args, **kwargs):
        user_agent = get_user_agent(self.request)

        if user_agent.is_pc:
            return redirect('desktop-videos')
        if check_validity(self.request):
            if check_validity(self.request) == 'failed':
                return redirect('login')
            if check_user(self.request) == '0':
                return super().dispatch(*args, **kwargs)
            else:
                messages.error(self.request, 'برجاء الإشتراك فى الخدمة')
                return redirect('login')
        return redirect('sms')

